# **Collaborate - Online Api**


This project uses [Google Firebase web platform](https://firebase.google.com/)

To run our project locally, clone this repository.

Run `npm install` in the main folder directory.

Please use latest version of `node`.

Run `npm start` to run the application locally and visit `[localhost:4000]` to explore.

#### Special thanks goes to: 

Our wonderful mentor - Zvezdelina Djokova<3

Our devoted trainers - Stoyan Peshev, Nadya Atanasova, Petya Grozdarska

#### What helped in our research and coding:

- [React.js Documentation](https://reactjs.org/docs/getting-started.html)
- [The official Firebase YouTube channel](https://www.youtube.com/user/Firebase)
- StackOverflow, Reddit and Medium



## **Project overview**

This project was built, using Google Firebase, as backend service and React 16.13.1 as front-end.

#### Additional packages used:

- [Konva](https://konvajs.org/docs/react/Intro.html)
- [Formik](https://formik.org/docs/overview)
- [Draftjs](https://draftjs.org/docs/getting-started)
- [React-beautiful-dnd](https://egghead.io/courses/beautiful-and-accessible-drag-and-drop-with-react-beautiful-dnd)

#### Styling:

- [Material-UI](https://material-ui.com/getting-started/installation/)
- [FontAwesome](https://fontawesome.com/)
- [Sass](https://sass-lang.com/)

## Application flow

Once you enter the provided website/localhost:4000/home app you can visit the homepage, sign in and sign up pages.

![](/client/images/homepage.png)

All other content is only available for registered users.

You have two options to sign up - email, password and Full name or Google login. 

![](./client/images/signup.png)


Once you've chosen your preferred method of registration, you can sign in. 


![](./client/images/signin.png)


Once signed in, you have full access of the functionality - create a canvas and share it among everyone on the platform or make it private and choose exactly who you want to collaborate with. Canvases give the opportunity to sketch using pencil, erase or even add text. All of the functionalities have aditional feature such as color, thickness or font chane. Futhermore, you can use manage your project using the kanban functionality. It gives you the advantage to add issue cards, with priorities, due dates, descriptions and etc. Users can be assigned to different cards and can also comment and even check thei progess.

![](./client/images/services.png)

![](./client/images/canvas.png)

![](./client/images/kanban.png)
Have fun exploring our app!

*We put a lot of work, passion and emotions in it!* 

You can contact us at  `ivan.tsankow[at]gmail` and `vladilen.panaiotov[at]gmail`
