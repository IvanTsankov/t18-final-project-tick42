import { createContext } from "react";

const ChatContext = createContext({
  chat: {
    open: false,
    name: 'General',
    id: 'generalkj12bhk3b12k3h2J121',
    service: ''
  },
  setChat: () => {}
});
export default ChatContext;