import { createContext } from 'react';

const getPrefColorScheme = () => {
    if (!window.matchMedia) return;
    return window.matchMedia("(prefers-color-scheme: dark)").matches;
}
export const getInitialMode = () => {
    const savedMode = JSON.parse(localStorage.getItem('dark'));
    const userPrefersDark = getPrefColorScheme();
    return savedMode || userPrefersDark || false;
}

const DarkModeContext = createContext({
    darkMode: false,
    setDarkMode: () => {},
});

export default DarkModeContext;