import React, { useEffect } from "react";
import { Layer } from "react-konva";
import TextField from "./TextField";
import application from "./../../base";
import * as toastr from 'toastr';

const AllTexts = ({
  setCurrentText,
  setTextEditVisible,
  setTexts,
  texts,
  textEditVisible,
  currentText,
  selected,
  setSelected,
  elements,
  setElements,
  updateElement,
  currentZoom,
  boardId
}) => {
  useEffect(() => {
    const fetchDataRealTime = async () => {
      await application
        .firestore()
        .collection("texts")
        .where('boardId', '==', boardId)
        .onSnapshot(querySnapshot => {
          const arr=[];
          querySnapshot.forEach(doc => {
            if (!doc.data().isDeleted) {
              arr.push({...doc.data(), id: doc.id});
            }
          });
          setTexts(arr);
        }, (err) => toastr.error(err));
    };
    fetchDataRealTime();

    return () =>
      application
        .firestore()
        .collection('texts')
        .onSnapshot(() => {});
  }, []);

  const updateTextsPosition = (texts) => {
        const newTexts = texts.slice().map((element) => {
          let newElement = {
            ...element,
            x: element.x*currentZoom,
            y: element.y*currentZoom,
            fontSize: element.fontSize*currentZoom,
          }
          return newElement;
    });
    return newTexts;
  }

  return (
    <Layer>
      {updateTextsPosition(texts).map((text, i) => {
        return (
          <TextField
          key={i}
            shapeProps={text}
            color={text.color}
            isSelected={text.id === selected.id}
            onSelect={e => {
              setSelected(text);
            }}
            onChange={newAttrs => {
              const inner = texts.slice();
              inner[i] = {...newAttrs};
              setElements([...elements, inner[i]])
              updateElement(inner[i], 'texts');
            }}
            setTextEditVisible={setTextEditVisible}
            setCurrentText={setCurrentText}
            textEditVisible={textEditVisible}
            currentText={currentText}
            setSelected={setSelected}
            currentZoom={currentZoom}
          />
        );
      })}
    </Layer>
  );
};

export default AllTexts;
