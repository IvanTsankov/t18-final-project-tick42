import React, { useEffect } from 'react';
import { Layer, Line } from 'react-konva';
import application from "./../../base";
import * as toastr from 'toastr';

const AllDrawings = ({
    drawings,
    setDrawings,
    currentZoom,
    boardId,
}) => {
    
    useEffect(() => {
    const fetchDataRealTime = async () => {
      await application
        .firestore()
        .collection("drawings")
        .where('boardId', '==', boardId)
        .onSnapshot(querySnapshot => {
          const arr =[]
          querySnapshot.forEach(doc => {
            if (!doc.data().isDeleted) {
              arr.push({...doc.data(), id: doc.id});
            }
          }, (err) => toastr.error(err));
          setDrawings(arr)
        });
    };

    fetchDataRealTime();

    return () =>
      application
        .firestore()
        .collection("drawings")
        .onSnapshot(() => {});
  }, []);

const updateDrawingsPosition = (drawings) => {
  const newDrawings = drawings.slice().map((element) => {
    let newElement = {
      ...element,
      coordinates: [...element.coordinates.map((coord) => coord*currentZoom)],
      lineWidth: element.lineWidth*currentZoom,
    }
    return newElement;
  });
  return newDrawings;
}

    return (  
        <Layer>
            {updateDrawingsPosition(drawings).map(drawing => 
                <Line
                draggable={false}
                points={drawing.coordinates}
                key={drawing.id}
                stroke={drawing.color}
                strokeWidth={drawing.lineWidth}
                lineJoin={drawing.lineJoin}
                lineCap={drawing.lineJoin}
                />
            )}
        </Layer>
    );
}

export default AllDrawings;