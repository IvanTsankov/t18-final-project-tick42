import React, { useState, useEffect } from "react";
import { Image, Layer } from "react-konva";
import application from "../../base";

const Drawing = ({
  tool,
  setDrawing,
  drawing,
  drawings,
  elements,
  setElements,
  currentColor,
  currentThickness,
  undoCounter,
  setUndoCounter,
  currentZoom,
  boardId,
}) => {
  const user = JSON.parse(localStorage.getItem('user'));
  const [state, setState] = useState({
    canvas: null,
    context: null
  });
  const [image, setImage] = useState(null);
  const [lastPointerPosition, setLastPointerPosition] = useState([]);

  const [isDrawing, toggleIsDrawing] = useState(false);

  useEffect(() => {
    const canvas = document.createElement("canvas");
    canvas.width = window.innerWidth-30;
    canvas.height = window.innerHeight-150;
    const context = canvas.getContext("2d");
    setState({ canvas, context });
  }, []);


  const createDrawing = async (element) => {
    setUndoCounter(undoCounter + 1);
    let newElement= {
      ...element,
      coordinates: [...element.coordinates.map((coord) => coord/currentZoom)],
      lineWidth: element.lineWidth/currentZoom,
  }
    await application
    .firestore()
    .collection('drawings')
    .add(newElement)
    .then((el) => setElements([...elements, {...newElement, id: el.id}]));
  }


  const handleMouseDown = () => {
    toggleIsDrawing(true);
    const stage = image.getStage();
    setLastPointerPosition(getRelativePointerPosition(stage));
  };
  
  const handleMouseUp = () => {
    state.context.clearRect(0, 0, state.canvas.width, state.canvas.height);
    setState({...state, context: state.context})
    toggleIsDrawing(false);
    createDrawing(drawing);
    setDrawing({
      coordinates: [],
      color: currentColor,
      lineJoin: "",
      lineWidth: currentThickness,
      time: new Date(),
      isDeleted: false,
      boardId: boardId,
      userId: user.user.uid
    });
    
  };

  const getRelativePointerPosition = (node) => {
    var transform = node.getAbsoluteTransform().copy();
    transform.invert();
    var pos = node.getStage().getPointerPosition();
    return transform.point(pos);
  }

  const handleMouseMove = () => {
    const { context } = state;
    setLastPointerPosition([]);

    if (isDrawing) {
      context.strokeStyle = tool === "drawing" ? drawing.color : "#ffffff";
      context.lineJoin = "round";
      context.lineWidth = drawing.lineWidth;

      context.beginPath();

      var localPos = {
        x: lastPointerPosition.x - image.x(),
        y: lastPointerPosition.y - image.y()
      };
      context.moveTo(localPos.x, localPos.y);
      setDrawing({
        ...drawing,
        color: state.context.strokeStyle,
        lineJoin: state.context.lineJoin,
        lineWidth: state.context.lineWidth,
        coordinates: [...drawing.coordinates, localPos.x, localPos.y]
      });

      const stage = image.getStage();

      var pos = getRelativePointerPosition(stage);

      localPos = {
        x: pos.x - image.x(),
        y: pos.y - image.y()
      };
      context.lineTo(localPos.x, localPos.y);
      context.closePath();
      context.stroke();
      setLastPointerPosition(pos);
      image.getLayer().draw();
    }
  };

  return (
    <Layer>
      <Image
        image={state.canvas}
        ref={node => setImage(node)}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onMouseMove={handleMouseMove}
        onTouchStart={handleMouseDown}
        onTouchMove={handleMouseMove}
        onTouchEnd={handleMouseUp}
      />
    </Layer>
  );
};

export default Drawing;
