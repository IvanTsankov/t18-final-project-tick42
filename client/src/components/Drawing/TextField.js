import React, { Fragment, useEffect } from "react";
import { Text, Transformer } from "react-konva";

const TextField = ({
  shapeProps,
  isSelected,
  onSelect,
  onChange,
  setTextEditVisible,
  setCurrentText,
  textEditVisible,
  currentText,
  setSelected,
  color,
}) => {
  const shapeRef = React.useRef();
  const trRef = React.useRef();

  useEffect(() => {
    if (isSelected) {
      trRef.current.nodes([shapeRef.current]);
      trRef.current.getLayer().batchDraw();
    }
  }, []);

  const handleTextDblClick = e => {
    setCurrentText(shapeProps);
    setTextEditVisible(true);
    setSelected({});
  };

  return (
    <Fragment
      key={shapeProps.id}
    >
      <Text
        visible={
          textEditVisible && currentText.id === shapeProps.id ? false : true
        }
        text={shapeProps.text}
        fill={color}
        fontSize={shapeProps.fontSize}
        onClick={onSelect}
        onTap={onSelect}
        ref={shapeRef}
        {...shapeProps}
        draggable
        onDragEnd={e => {
          onChange({
            ...shapeProps,
            x: e.target.x(),
            y: e.target.y()
          });
        }}
        onk
        onDblClick={handleTextDblClick}
        onTransformEnd={e => {
          const node = shapeRef.current;
          const scaleX = node.scaleX();
          const scaleY = node.scaleY();

          node.scaleX(1);
          node.scaleY(1);
          onChange({
            ...shapeProps,
            x: node.x(),
            y: node.y(),
            width: Math.max(5, node.width() * scaleX),
            height: Math.max(node.height() * scaleY)
          });
        }}
      />
      {isSelected && (
        <Transformer
          ref={trRef}
          boundBoxFunc={(oldBox, newBox) => {
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </Fragment>
  );
};

export default TextField;
