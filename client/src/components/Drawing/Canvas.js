import React, { useState, useEffect, useContext } from "react";
import { Stage } from "react-konva";
import Drawing from "./Drawing";
import AllTexts from "./AllTexts";
import AllDrawings from "./AllDrawings";
import Toolbar from "../Base/ToolBar/Toolbar";
import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";
import application from "./../../base";
import "./Canvas.scss";
import MouseSharing from "../Pages/Services/MouseSharing";
import * as toastr from "toastr";
import ChatContext from "../../providers/ChatContext";
import drawIcon from './../../images/drawToolButton.svg';
import eraserIcon from './../../images/eraser.svg';
import textIcon from './../../images/text.svg';
import undoIcon from './../../images/undo.svg';
import redoIcon from './../../images/redo.svg';
import pdfIcon from './../../images/pdf.svg';
import mouseIcon from './../../images/mouse.svg';
import zoomIcon from './../../images/zoom.svg';
import DarkModeContext from "../../providers/DarkModeContext";

const Canvas = props => {
  const {darkMode} = useContext(DarkModeContext);
  const { setChat } = useContext(ChatContext);
  const user = JSON.parse(localStorage.getItem("user"));
  const [boardId, setBoardId] = useState(props.location.pathname.substring(10));
  const [selected, setSelected] = useState({});
  const [tool, setTool] = useState("");
  const [undone, setUndone] = useState([]);
  const [texts, setTexts] = useState([]);
  const [elements, setElements] = useState([]);
  const [drawings, setDrawings] = useState([]);
  const [currentThickness, setCurrentThickness] = useState(1);
  const [currentColor, setCurrentColor] = useState("000000");
  const [showScale, setShowScale] = useState(false);
  const [drawing, setDrawing] = useState({
    coordinates: [],
    color: currentColor,
    lineJoin: "",
    lineWidth: currentThickness,
    time: new Date(),
    isDeleted: false,
    boardId: boardId,
    createdBy: user.user.uid
  });
  const [textEditVisible, setTextEditVisible] = useState(false);
  const [currentText, setCurrentText] = useState({});
  const stageEl = React.createRef();
  const [currentZoom, setCurrentZoom] = useState(1);
  const [undoCounter, setUndoCounter] = useState(0);
  const [redoCounter, setRedoCounter] = useState(0);
  const [canvasTitle, setCanvasTitle] = useState('');
  

  useEffect(() => {
    let arrNew = [];
    const fetchElements = async () => {
      const db = application.firestore();
      await db
        .collection("texts")
        .where("boardId", "==", boardId)
        .orderBy("time")
        .get()
        .then(data => {
          const arr = data.docs.map(doc => ({ ...doc.data(), id: doc.id }));
          arrNew = [...arrNew, ...arr.filter(el => !el.isDeleted)];
        })
        .then(async () => {
          const db = application.firestore();
          await db
            .collection("drawings")
            .where("boardId", "==", boardId)
            .orderBy("time")
            .get()
            .then(data => {
              const arr = data.docs.map(doc => ({ ...doc.data(), id: doc.id }));
              arrNew = [...arrNew, ...arr.filter(el => !el.isDeleted)];
            });
        })
        .then(() => setElements(arrNew));
    };

    const fetchCanvas = async () => {
      await application
        .firestore()
        .collection("canvases")
        .doc(boardId)
        .get()
        .then(data => {
          if (!data.data()) {
            props.history.push("/notfound");
          }
          else if (
            data.data().createdBy !== user.user.uid &&
            data.data().invitedUsers.indexOf(user.user.uid) === -1 &&
            !data.data().isPublic
          ) {
            props.history.push("/notfound");
          }
          setChat({ open: false, name: data.data().name, id: boardId });
          setCanvasTitle(data.data().name);
        })
        .catch(err => {
          toastr.error(err);
          props.history.push("/notfound");
        });
    };
    fetchCanvas();
    fetchElements();

    return async () => {
      await application
        .firestore()
        .collection("mouses")
        .doc(user.user.uid)
        .delete();
        setChat({open: false, name: "General", id: 'generalkj12bhk3b12k3h2J121'})
    };
  }, []);
  const updateElement = async (element, doc) => {
    setUndoCounter(undoCounter + 1);
    let newElement = {
      ...element,
      x: element.x / currentZoom,
      y: element.y / currentZoom,
      fontSize: element.fontSize / currentZoom
    };
    await application
      .firestore()
      .collection(doc)
      .doc(newElement.id)
      .set(newElement);
  };
  const deleteElement = async (element, doc) => {
    setUndoCounter(undoCounter + 1);
    await application
      .firestore()
      .collection(doc)
      .doc(element.id)
      .set(element);
  };

  const recoverText = async element => {
    setUndoCounter(undoCounter + 1);
    await application
      .firestore()
      .collection("texts")
      .doc(element.id)
      .set(element);
  };

  const createText = async element => {
    setUndoCounter(undoCounter + 1);
    let newElement = {
      ...element,
      x: element.x / currentZoom,
      y: element.y / currentZoom,
      fontSize: element.fontSize / currentZoom
    };
    await application
      .firestore()
      .collection("texts")
      .add(newElement)
      .then(el => setElements([...elements, { ...newElement, id: el.id }]));
  };

  const handleTextEdit = e => {
    setCurrentText({
      ...currentText,
      text: e.target.value
    });
  };

  const handleTextareaKeyDown = (e, fromMouseDown = false) => {
    if (e.keyCode === 13 || fromMouseDown) {
      if (currentText.text.length === 0) {
        currentText.isDelete = true;
        setTexts([...texts.filter(el => !el.isDeleted)]);
        deleteElement(currentText, "texts");
      } else {
        updateElement(currentText, "texts");
      }
      setElements([...elements, currentText]);
      setTextEditVisible(false);
      updateElement(currentText, "texts");
    }
  };

  const checkDeselect = e => {
    const clickedOnEmpty = e.target === e.target.getStage();
    if (textEditVisible && clickedOnEmpty) {
      handleTextareaKeyDown(e, true);
    } else if (clickedOnEmpty) {
      setSelected({});
    }
  };

  const handleAddText = () => {
    setTool("text");
    const newText = {
      text: "Edit text",
      x: 50,
      y: 50,
      fontFamily: "Calibri",
      fontSize: 20,
      color: "000000",
      boardId: boardId,
      isDeleted: false,
      time: new Date(),
      userId: user.user.uid
    };
    setUndone([...undone.slice(0, undone[undone.length - 1])]);
    createText(newText);
  };

  const handleUndo = () => {
    if (elements.length === 0) {
      return;
    }
    if (!elements[elements.length - 1].coordinates) {
      const check = elements
        .slice()
        .filter(el => el.id === elements[elements.length - 1].id);
      if (1 < check.length) {
        setUndone([...undone, check[check.length - 1]]);
        updateElement(check[check.length - 2], "texts");
      } else {
        const el = elements[elements.length - 1];
        el.isDeleted = true;
        deleteElement(el, "texts");
        setUndone([...undone, el]);
        setTool("");
      }
    } else {
      setUndone([...undone, drawings[drawings.length - 1]]);
      drawings[drawings.length - 1].isDeleted = true;
      deleteElement(drawings[drawings.length - 1], "drawings");
    }
    elements.length -= 1;
    setElements([...elements]);
    setUndoCounter(undoCounter - 1);
    setRedoCounter(redoCounter + 1);
  };

  const handleRedo = () => {
    if (undone.length === 0) {
      return;
    }
    if (undone[undone.length - 1].coordinates) {
      undone[undone.length - 1].isDeleted = false;
      updateElement(undone[undone.length - 1], "drawings");
      setDrawings([...drawings, undone[undone.length - 1]]);
    } else {
      const check = texts
        .slice()
        .find(el => el.id === elements[elements.length - 1]?.id);
      if (check) {
        const newTexts = texts.slice();
        const index = newTexts.indexOf(check);
        newTexts[index] = undone[undone.length - 1];
        updateElement(newTexts[index], "texts");
        setUndone([...undone]);
      } else {
        undone[undone.length - 1].isDeleted = false;
        recoverText(undone[undone.length - 1]);
      }
    }
    setElements([...elements, undone[undone.length - 1]]);
    undone.length -= 1;
    setUndone([...undone]);
    setTool("");
    setUndoCounter(undoCounter + 1);
    setRedoCounter(redoCounter - 1);
  };

  const handleWheel = e => {
    if (tool !== "") return;
    e.evt.preventDefault();
    if (e.evt.deltaY > 0) {
      if (currentZoom === 0.9875) setCurrentZoom(1);
      else setCurrentZoom(Math.min(+currentZoom + 0.025, 3).toFixed(4));
    } else {
      if (currentZoom > 1)
        setCurrentZoom(Math.max(+currentZoom - 0.025, 0.5).toFixed(4));
      else setCurrentZoom(Math.max(+currentZoom - 0.0125, 0.5).toFixed(4));
    }
  };

  const handleSave = () => {
    html2canvas(document.getElementById("stageDiv")).then(canvas => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF("l", "px", [1041, 426]);
      pdf.addImage(imgData, "PNG", 0, 0);
      pdf.save(`drawing.pdf`);
    });
  };

  const handleMouseMove = async e => {
    await application
      .firestore()
      .collection("mouses")
      .doc(user.user.uid)
      .set({
        x: e.evt.clientX,
        y: e.evt.clientY,
        pageId: boardId,
        avatar: localStorage.getItem('imgUrl')
      });
  };
  
  return (
    <>
    <h1 style={{textAlign: 'center', fontSize: '40px', color: darkMode ? '#F0F2F5' : '#18191A'}}>{canvasTitle}</h1>
    <div
      onContextMenu={e => e.preventDefault()}
      id="stage-parent"
      style={{
        marginLeft: "10px",
        width: window.innerWidth - 60,
        height: window.innerHeight - 110,
        overflow: "auto"
      }}
    >
      <button
        className="canvasButton"
        onClick={() => {
          setTool("drawing");
          setUndone([...undone.slice(0, undone[undone.length - 1])]);
        }}
      >
        <img alt="draw" src={drawIcon}/>
      </button>
      <button className="canvasButton" onClick={handleAddText}>
      <img alt="text" src={textIcon}/>
      </button>
      <button
        className="canvasButton"
        onClick={() => {
          setTool("erase");
          setUndone([...undone.slice(0, undone[undone.length - 1])]);
        }}
      >
        <img alt="erase" src={eraserIcon}/>
      </button>
      <button className="canvasButton" onClick={() => setTool("")} >
        <img alt="mouse" src={mouseIcon}/>
      </button>
      <button
        className="canvasButton"
        disabled={!undoCounter}
        onClick={handleUndo}
      >
        <img alt="undo" src={undoIcon}/>
      </button>
      <button
        className="canvasButton"
        disabled={!redoCounter}
        onClick={handleRedo}
      >
        <img alt="redo" src={redoIcon}/>
      </button>
      <button className="canvasButton" onClick={handleSave}>
      <img alt="pdf" src={pdfIcon} />
      </button>
      <button className="canvasButton" onClick={() => setShowScale(!showScale)}>
        <img alt="zoom" src={zoomIcon}/>
        <span style={{color: darkMode ? '#F0F2F5' : '#242526'}}>{Number(currentZoom * 100).toFixed(0) + "%"}</span>
      </button>
      {showScale &&
      <input
      type="range"
      min="50"
      max="300"
      value={currentZoom*100}
    
      onChange={(e) => setCurrentZoom(e.target.value/100)}
    />
      }
      {(tool !== "" || selected.id) && (
        <Toolbar
          tool={tool}
          setTool={setTool}
          selected={selected}
          setSelected={setSelected}
          texts={texts}
          setTexts={setTexts}
          elements={elements}
          setElements={setElements}
          setDrawing={setDrawing}
          drawing={drawing}
          setCurrentThickness={setCurrentThickness}
          currentThickness={currentThickness}
          setCurrentColor={setCurrentColor}
          currentColor={currentColor}
          updateElement={updateElement}
        />
      )}
      <br />
      <br />
      <div id="stageDiv">
        <Stage
          width={window.innerWidth - 70}
          height={window.innerHeight - 180}
          onMouseDown={checkDeselect}
          onWheel={handleWheel}
          onMouseMove={handleMouseMove}
          style={{
            backgroundColor: "white",
            display: "inline-block",
            borderRadius: 3
          }}
          ref={stageEl}
        >
          <AllTexts
            setTextEditVisible={setTextEditVisible}
            setCurrentText={setCurrentText}
            setTexts={setTexts}
            texts={texts}
            textEditVisible={textEditVisible}
            currentText={currentText}
            selected={selected}
            setSelected={setSelected}
            setElements={setElements}
            elements={elements}
            updateElement={updateElement}
            currentZoom={currentZoom}
            boardId={boardId}
          />
          <AllDrawings
            drawings={drawings}
            setDrawings={setDrawings}
            currentZoom={currentZoom}
            boardId={boardId}
          />
          {(tool === "drawing" || tool === "erase") && (
            <Drawing
            tool={tool}
            setDrawing={setDrawing}
            drawing={drawing}
            drawings={drawings}
            setDrawings={setDrawings}
            elements={elements}
            setElements={setElements}
            currentColor={currentColor}
            currentThickness={currentThickness}
            undoCounter={undoCounter}
            setUndoCounter={setUndoCounter}
            currentZoom={currentZoom}
            boardId={boardId}
            />
            )}
        </Stage>
          <MouseSharing pageId={boardId} userId={user.uid} />
      </div>
      <textarea
        value={currentText.text}
        style={{
          color: currentText.color,
          display: textEditVisible ? "block" : "none",
          top: currentText.y + 118 + "px",
          left: currentText.x + 15 + "px",
          position: "absolute",
          border: "none",
          padding: "0px",
          margin: "0px",
          outline: "none",
          resize: "none",
          background: "none",
          fontSize: currentText.fontSize,
          fontFamily: currentText.fontFamily
        }}
        onChange={handleTextEdit}
        onKeyDown={handleTextareaKeyDown}
      />
    </div>
  </>
  );
};

export default Canvas;
