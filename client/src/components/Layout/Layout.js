import React, { Fragment, useContext } from 'react';
import Navigation from '../Base/Navigation/Navigation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComment } from '@fortawesome/free-solid-svg-icons';
import Chat from './../Base/Chat/Chat';
import ChatContext from '../../providers/ChatContext';
import AuthContext from '../../providers/AuthContext';
import './Layout.scss';

const Layout = props => {
    const { chat, setChat } = useContext(ChatContext);
    const { user } = useContext(AuthContext);
    return (
    <Fragment>
        <Navigation />
        <main>{props.children}
        {user ? chat.open ? <Chat /> : <button onClick={() => setChat({open: true, name: chat.name, id: chat.id})} className="chatButton"><FontAwesomeIcon icon={faComment} flip="horizontal" size="4x" /></button> : null}
        </main>
    </Fragment>
    );
};

export default Layout;