import React from 'react';
import { Formik, useField, Form } from "formik";
import * as Yup from "yup";
import application from "../../../../base";
import * as toastr from "toastr";
import passwordImg from './../../../../images/password.svg'

const RecoverPassword = ({handleModalClose, recoverPassword, setRecoverPassword}) => {

    const CustomTextInput = ({ label, ...props }) => {
        const [field, meta] = useField(props);
        return (
          <>
            <input className="text-input" {...field} {...props} />
            {meta.touched && meta.error ? (
              <div className="error">{meta.error}</div>
            ) : null}
          </>
        );
      };

    return (
        <div className="form">
            <div className="form-group">
              <Formik
                initialValues={{
                  email: "",
                }}
                validationSchema={Yup.object({
                  email: Yup.string()
                    .email("Invalid email address")
                    .required("Required"),
                })}
                onSubmit={async (values, { setSubmitting, resetForm }) => {
                  try {
                    await application.auth().sendPasswordResetEmail(values.email);
                    resetForm();
                    setSubmitting(false);
                    toastr.success('You have reset your password successfully!')
                    handleModalClose();
                  } catch (error) {
                    resetForm();
                    setSubmitting(false);
                    toastr.error(error.message);
                  } 
                }}
              >
                {(props) => (
                  <Form style={{marginTop: '0px'}}>
                    <div className="imageRecoverPassword">
                      <img alt="password" src={passwordImg} />
                    </div>
                    <CustomTextInput
                      name="email"
                      type="email"
                      placeholder="Enter your email"
                    />
                    <button type="submit" className="logRegButton">
                        {props.isSubmitting ? 'Loading...' : 'Submit'}
                    </button>
                    <div className="signup-link" style={{alignSelf: 'center'}}> 
                      <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} onClick={() => setRecoverPassword(false)}>Back to Sign in</span>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
    )
}

export default RecoverPassword;