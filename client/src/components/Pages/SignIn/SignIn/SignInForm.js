import React, { useContext } from "react";
import { Formik, useField, Form } from "formik";
import * as Yup from "yup";
import application from "../../../../base";
import firebase from "firebase";
import AuthContext from "../../../../providers/AuthContext";
import * as toastr from "toastr";

const SignInForm = ({
  handleModalClose,
  setRecoverPassword,
  setIsLoginActive
}) => {
  const { setUser } = useContext(AuthContext);

  const saveUserData = async(r, img) => {
    await application
          .firestore()
          .collection("users")
          .doc(r.user.uid)
          .set({
            name: r.user.displayName,
            email: r.user.email,
            avatar: img,
          });
  }

  const fetchPhoto = async(r) => {
    const user = JSON.parse(localStorage.getItem('user')).user;
    await application
      .storage()
      .ref(`users/${user?.uid}/profile.jpg`)
      .getDownloadURL()
      .then(img => {
        saveUserData(r, img);
        localStorage.setItem('imgUrl', img);
      })
      .catch(async e => {
        await application
          .storage()
          .ref(`/default/default.png`)
          .getDownloadURL()
          .then(img => {
            localStorage.setItem('imgUrl', img);
            saveUserData(r, img);
          })
          .catch((e) => {});
      });
  }

  
  const googleAuth = async () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope("https://www.googleapis.com/auth/plus.login");

    await application
      .auth()
      .signInWithPopup(provider)
      .then(async r => {
        localStorage.setItem("user", JSON.stringify(r));
        setUser(JSON.stringify(r));
        toastr.success("You have logged in successfully!");
        fetchPhoto(r)
        handleModalClose();
      })
      .catch(error => {
        toastr.error(error.message);
      });
  };

  const facebookAuth = async () => {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope("user_birthday");

    await application
      .auth()
      .signInWithPopup(provider)
      .then(r => {
        localStorage.setItem("user", JSON.stringify(r));
        setUser(JSON.stringify(r));
        toastr.success("You have logged in successfully!");
        fetchPhoto(r);
        handleModalClose();
      })
      .catch(error => {
        toastr.error(error.message);
      });
  };

  const CustomTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };

  return (
    <div className="form">
      <div className="form-group">
        <Formik
          initialValues={{
            email: "",
            password: ""
          }}
          validationSchema={Yup.object({
            email: Yup.string()
              .email("Invalid email address")
              .required("Required"),
            password: Yup.string()
              .min(8, "Must be at least 8 characters")
              .max(20, "Must be 20 characters or less")
              .required("Required")
          })}
          onSubmit={async (values, { setSubmitting, resetForm }) => {
            try {
              await application
                .auth()
                .signInWithEmailAndPassword(values.email, values.password)
                .then((r) => {
                  localStorage.setItem("user", JSON.stringify(r));
                  setUser(JSON.stringify(r));
                  fetchPhoto(r)
                });
              toastr.success("You have logged in successfully!");
              resetForm();
              setSubmitting(false);
              handleModalClose();
            } catch (error) {
              resetForm();
              setSubmitting(false);
              toastr.error(error.message);
            }
          }}
        >
              {(props) => (
                  <Form>
                    <CustomTextInput
                      name="email"
                      type="email"
                      placeholder="Enter your email"
                    />
                    <CustomTextInput
                      name="password"
                      type="password"
                      placeholder="Enter your password"
                    />
                    <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} className="forgotPassword" onClick={() => setRecoverPassword(true)}>Forgot password?</span>
                    <button type="submit" className="logRegButton">
                        {props.isSubmitting ? 'Loading...' : 'Sign in'}
                    </button>
                  </Form>
                )}
              </Formik>
            </div>
                <div className="signup-link">Or sign in with</div>
                <div className="logWithButtons">
                  <button className="logRegButton" onClick={googleAuth} style={{backgroundColor: 'transparent', border: '1px solid #21b573', marginRight: '5px' }}>
                    <img alt="logo" src="https://img.icons8.com/color/64/000000/google-logo.png" style={{height: 'auto', width: '30px', left: '-10px'}} />
                    <span>Google</span>
                  </button>
                  <button className="logRegButton" onClick={facebookAuth} style={{backgroundColor: 'transparent', border: '1px solid #21b573'}}>
                    <img alt="logo" src="https://img.icons8.com/fluent/48/000000/facebook-new.png" style={{height: 'auto', width: '30px'}} />
                    <span>Facebook</span>
                  </button>
                </div>
                <div className="signup-link">Not a member? 
                  <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} onClick={() => setIsLoginActive(false)}>Sign up</span>
                </div>
          </div>
    )
}

export default SignInForm;
