import React, { useState } from "react";
import "./SignIn.scss";
import SignInForm from "./SignInForm";  
import RecoverPassword from "./RecoverPassword";

const SignIn = ({ handleModalClose, setIsLoginActive }) => {
  const [recoverPassword, setRecoverPassword] = useState(false);

  return (
      <div className="logRegInnerContainer">
        <div className={`header ${recoverPassword ? 'recoverPassword' : ''}`}>{recoverPassword ? 'Recover password' : 'Sign in'}</div>
        <div className="content">
          {recoverPassword 
          ? <RecoverPassword handleModalClose={handleModalClose} recoverPassword={recoverPassword} setRecoverPassword={setRecoverPassword} /> 
          : <SignInForm handleModalClose={handleModalClose} recoverPassword={recoverPassword} setRecoverPassword={setRecoverPassword} setIsLoginActive={setIsLoginActive} />}
        </div>
      </div>
  );
};

export default SignIn;
