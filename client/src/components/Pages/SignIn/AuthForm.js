import React, { useState } from 'react';
import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import './AuthForm.scss';

const AuthForm = ({ handleModalClose }) => {
    const [isLoginActive, setIsLoginActive] = useState(true);
    return (
          <div className="logReg">
            <div className="logRegOuterContainer">
              {isLoginActive ? <SignIn handleModalClose={handleModalClose} setIsLoginActive={setIsLoginActive} /> : <SignUp setIsLoginActive={setIsLoginActive} />}
            </div>
          </div>
      );
}

export default AuthForm;