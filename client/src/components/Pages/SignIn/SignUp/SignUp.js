import React from "react";
import "./SignUp.scss";
import { Formik, useField, Form } from "formik";
import * as Yup from "yup";
import application from "../../../../base";
import * as toastr from "toastr";

const Register = ({ setIsLoginActive }) => {
  const CustomTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };

  return (
      <div className="logRegInnerContainer">
        <div className="header">Sign up</div>
        <div className="content">
          <div className="form">
            <div className="form-group">
              <Formik
                initialValues={{
                  email: "",
                  fullname: "",
                  password: "",
                  confirmPassword: ""
                }}
                validationSchema={Yup.object({
                  email: Yup.string()
                    .email("Invalid email address")
                    .required("Required"),
                  fullname: Yup.string()
                  .min(4, "Must be at least 4 characters")
                  .max(15, "Must be 15 characters or less")
                  .required("Required"),
                  password: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .required("Required"),
                  confirmPassword: Yup.string()
                    .min(8, "Must be at least 8 characters")
                    .max(20, "Must be 20 characters or less")
                    .required("Required")
                    .oneOf(
                      [Yup.ref("password")],
                      "Both passwords must be the same!"
                    )
                })}
                onSubmit={async (values, { setSubmitting, resetForm }) => {
                  try {
                    await application
                      .auth()
                      .createUserWithEmailAndPassword(
                        values.email,
                        values.password
                      )
                      .then((res) => {
                        const user = application.auth().currentUser;
                        return user.updateProfile({
                          displayName: values.fullname
                        })
                      })
                    resetForm();
                    setSubmitting(false);
                    setIsLoginActive(true);
                    toastr.success("You have registered successfully!");
                  } catch (error) {
                    resetForm();
                    setSubmitting(false);
                    toastr.error(error.message);
                  }
                }}
              >
                {props => (
                  <Form>
                    <CustomTextInput
                      name="email"
                      type="email"
                      placeholder="Enter your email"
                    />
                    <CustomTextInput
                      name="fullname"
                      type="text"
                      placeholder="Enter your full name"
                    />
                    <CustomTextInput
                      name="password"
                      type="password"
                      placeholder="Enter your password"
                    />
                    <CustomTextInput
                      name="confirmPassword"
                      type="password"
                      placeholder="Confirm your password"
                    />
                    <button type="submit" className="logRegButton" style={{marginTop: '15px'}}>
                        {props.isSubmitting ? 'Loading...' : 'Sign up'}
                    </button>
                  </Form>
                )}
              </Formik>
              <div className="signup-link" style={{marginTop: '40px'}}>Already a member? 
              <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} onClick={() => setIsLoginActive(true)}>Sign in</span>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Register;
