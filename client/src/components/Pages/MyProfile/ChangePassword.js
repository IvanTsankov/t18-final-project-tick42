import React from "react";
import { Formik, useField, Form } from "formik";
import * as Yup from "yup";
import application from "../../../base";
import * as toastr from "toastr";
import passwordImg from "./../../../images/password.svg";

const ChangePassword = ({ handleModalClose, setProfile }) => {
  const CustomTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
      <>
        <input className="text-input" {...field} {...props} />
        {meta.touched && meta.error ? (
          <div className="error">{meta.error}</div>
        ) : null}
      </>
    );
  };

  return (
    <>
      <div className="header">
        Change Password
      </div>
      <div className="content">
        <div className="imageProfile">
          <img alt="password" src={passwordImg} />
        </div>
        <div className="form">
          <div className="form-group">
            <Formik
              initialValues={{
                newPassword: "",
                confirmNewPassword: "",
              }}
              validationSchema={Yup.object({
                newPassword: Yup.string()
                  .min(8, "Must be at least 8 characters")
                  .max(20, "Must be 20 characters or less")
                  .required("Required"),
                confirmNewPassword: Yup.string()
                  .min(8, "Must be at least 8 characters")
                  .max(20, "Must be 20 characters or less")
                  .required("Required")
                  .oneOf(
                    [Yup.ref("newPassword")],
                    "Both passwords must be the same!"
                  ),
              })}
              onSubmit={async (values, { setSubmitting, resetForm }) => {
                try {
                  await application
                    .auth()
                    .currentUser.updatePassword(values.newPassword);
                  resetForm();
                  setSubmitting(false);
                  handleModalClose();
                  toastr.success(
                    "You have changed your password successfully!"
                  );
                } catch (error) {
                  resetForm();
                  setSubmitting(false);
                  toastr.error(error.message);
                }
              }}
            >
              {(props) => (
                <Form>
                  <CustomTextInput
                    name="newPassword"
                    type="password"
                    placeholder="Enter your new password"
                  />
                  <CustomTextInput
                    name="confirmNewPassword"
                    type="password"
                    placeholder="Confirm your new password"
                  />
                  <button
                    type="submit"
                    className="profileButton changePasswordButton"
                  >
                    {props.isSubmitting ? "Loading..." : "Submit"}
                  </button>
                </Form>
              )}
            </Formik>
            <div className="signup-link"> 
            <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} onClick={() => setProfile(true)}>Back to Profile</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ChangePassword;
