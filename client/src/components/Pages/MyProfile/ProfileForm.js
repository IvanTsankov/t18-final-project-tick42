import React, { useContext, useState } from 'react';
import AuthContext from '../../../providers/AuthContext';
import application from '../../../base';
import * as toastr from 'toastr';
import { Button } from '@material-ui/core';
import './MyProfile.scss';

const ProfileForm = ({ setProfile }) => {
    const { user } = useContext(AuthContext);
    const [test, setTest] = useState(false);

    const handlePhotoUpload = async e => {
      await application
      .storage()
      .ref(`users/${user.uid}/profile.jpg`)
      .put(e.target.files[0])
      .then(async () => {
        await application
        .storage()
        .ref(`users/${user.uid}/profile.jpg`)
        .getDownloadURL()
        .then(async (imgUrl) => {
          await application
          .firestore()
          .collection("users")
          .doc(user.uid)
          .set({
            name: user.displayName,
            email: user.email,
            avatar: imgUrl
          });
          localStorage.setItem('imgUrl', imgUrl)});
          setTest(!test);
      })
      .catch(e => toastr.error(e))
    };

    

return (
    <>
      <div className="content">
        <div>
          <img
          className="imageResponsive imageProfile"
          src={localStorage.getItem('imgUrl')}
          alt="profile"
          />
        </div>
        <div className="profileContent" style={{display: 'inline-block', marginLeft: '10px', height: '100%', margin: '20px 0px'}}>
          <div className="profileField profileDataAttr" style={{fontSize: '18px'}}>{user.displayName ? user.displayName : user.email.split('@')[0]}</div>
          <div className="profileField profileDataAttr" style={{marginTop: '10px'}}>{user.email}</div>
        </div>
            <Button
                  className="uploadButton"
                  component="label"
                >
                  Change Photo
                  <input
                    type="file"
                    onChange={handlePhotoUpload}
                    style={{ display: "none" }}
                  />
            </Button>
            <div className="signup-link"> 
              <span style={{color: '#347cdb', cursor: 'pointer', marginLeft: 5}} onClick={() => setProfile(false)}>Change Password</span>
            </div>

      </div>
    </>
)
}

export default ProfileForm;