import React, { useState } from "react";
import "./MyProfile.scss";
import ChangePassword from "./ChangePassword";
import ProfileForm from "./ProfileForm";
import { Grid } from "@material-ui/core";

const MyProfile = ({ handleModalClose}) => {
  const [profile, setProfile] = useState(true);


  return (
    <div className="profile">
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
      >
        <Grid item xs={12} sm={10} xl={10}>
      <div className="profileOuterContainer" style={{alignItems: 'normal'}}>
          <div className="profileInnerContainer">
            {profile 
              ? <ProfileForm setProfile={setProfile} handleModalClose={handleModalClose} />
              : <ChangePassword handleModalClose={handleModalClose} setProfile={setProfile} />}
          </div>
      </div>
      </Grid>
      </Grid>
    </div>
  );
};

export default MyProfile;
