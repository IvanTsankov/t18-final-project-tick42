import React from "react";
import { Button } from "@material-ui/core";
import './NotFound.scss';

const NotFound = ( props ) => {
  return (
    <div className="NotFound">
      <div className="textNotFound">
        <p>404</p>
      </div>
      <div className="containerNotFound">
        <div className="cavemanNotFound">
          <div className="legNotFound">
            <div className="footNotFound">
              <div className="fingersNotFound"></div>
            </div>
          </div>
          <div className="legNotFound">
            <div className="footNotFound">
              <div className="fingersNotFound"></div>
            </div>
          </div>
          <div className="shapeNotFound">
            <div className="circleNotFound"></div>
            <div className="circleNotFound"></div>
          </div>
          <div className="headNotFound">
            <div className="eyeNotFound">
              <div className="noseNotFound"></div>
            </div>
            <div className="mouthNotFound"></div>
          </div>
          <div className="arm-rightNotFound">
            <div className="clubNotFound"></div>
          </div>
        </div>
        <div className="cavemanNotFound">
          <div className="legNotFound">
            <div className="footNotFound">
              <div className="fingersNotFound"></div>
            </div>
          </div>
          <div className="legNotFound">
            <div className="footNotFound">
              <div className="fingersNotFound"></div>
            </div>
          </div>
          <div className="shapeNotFound">
            <div className="circleNotFound"></div>
            <div className="circleNotFound"></div>
          </div>
          <div className="headNotFound">
            <div className="eyeNotFound">
              <div className="noseNotFound"></div>
            </div>
            <div className="mouthNotFound"></div>
          </div>
          <div className="arm-rightNotFound">
            <div className="clubNotFound"></div>
          </div>
        </div>
      </div>
      <br/>
      <br/>
      <Button
                variant="contained"
                style={{height: '50px', width: '300px', fontSize: '17px', marginTop: '550px', marginLeft: '810px', opacity: '0.8', background: '#DE1A1A', color: '#FFFFFF', textTransform: 'none'}}
                onClick={() => props.history.push("/home")}
              >
                Go to Home Page
              </Button>
    </div>
  );
};
export default NotFound;
