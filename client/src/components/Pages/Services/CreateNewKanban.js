import React, { useState, useEffect, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import application from "../../../base";
import DarkModeContext from "../../../providers/DarkModeContext";
import { Button } from "@material-ui/core";
import * as toastr from "toastr";
import * as firebase from "firebase";

const CreateNewKanban = ({ kanbanToUpdate, setShowKanbanCreateForm, setKanbanToUpdate }) => {
  const { darkMode } = useContext(DarkModeContext);
  const user = JSON.parse(localStorage.getItem("user"));
  const [usersData, setUsersData] = useState([]);
  const [newKanban, setNewKanban] = useState(
    kanbanToUpdate
      ? kanbanToUpdate
      : {
          createdBy: user.user.uid,
          time: new Date(),
          invitedUsers: kanbanToUpdate
          ? usersData
              .filter(
                el => kanbanToUpdate?.invitedUsers?.indexOf(el.id) !== -1
              )
              .map(el => el.email)
          : [],
          isPublic: true,
          name: "",
          isDeleted: false
        }
  );

  useEffect(() => {
    const fetchUsersData = async () => {
      let arr = [];
      await application
        .firestore()
        .collection("users")
        .where("__name__", "!=", user.user.uid)
        .get()
        .then(data => {
          arr = data.docs.map(data => ({ ...data.data(), id: data.id }));
        }, err => toastr.error(err));
      setUsersData(arr);
    };

    fetchUsersData();

    return () => {
      setKanbanToUpdate(null);
      setNewKanban({});
    }
  }, []);

  const handleUpdate = async() => {
    const newInvites = newKanban.invitedUsers.filter(el => kanbanToUpdate.invitedUsers.indexOf(el) === -1)
    await application
      .firestore()
      .collection("kanbans")
      .doc(newKanban.id)
      .set({
        createdBy: newKanban.createdBy,
        time: newKanban.time,
        invitedUsers: newKanban.invitedUsers,
        name: newKanban.name,
        isDeleted: newKanban.isDeleted
      })
      .then((el) => {
        sendNotification(newInvites, kanbanToUpdate.id)
        setKanbanToUpdate(null);
        setNewKanban({ name: ''})
        setShowKanbanCreateForm(false); 
      });
  };

  const handleCreate = async () => {
    await application
      .firestore()
      .collection("kanbans")
      .add({...newKanban, invitedUsers: newKanban.invitedUsers})
      .then((el) => {
        sendNotification(newKanban.invitedUsers, el.id)
      })
      .then(() => {
        setKanbanToUpdate(null);
        setNewKanban({ name: ''})
        setShowKanbanCreateForm(false); 
      });
  };

  const sendNotification = (userIds, boardId) => {
    userIds.forEach(async(id) => {
      await application
          .firestore()
          .collection("notifications")
          .doc(id)
          .set({
            notifications: firebase.firestore.FieldValue.arrayUnion({
            kanbanId: boardId,
            content: `${user.user.displayName || user.user.email.split('@')[0]} shared Kanban board ${newKanban.name} with you!`,
            time: new Date(),
            isRead: false,

        })}, {merge: true})
    })
  }

  return (
    <>
      <h2>
        {0 < newKanban.name?.length ? newKanban.name : "New Drawing board"}
      </h2>
      <TextField
        value={newKanban.name}
        color="primary"
        id="name"
        label={<p style={{ color: darkMode ? "#F0F2F5" : "#18191A" }}>Name</p>}
        InputProps={{
          style: { color: darkMode ? "#F0F2F5" : "#18191A" }
        }}
        onChange={e => setNewKanban({ ...newKanban, name: e.target.value })}
      />
      <br />
      <br />
      {kanbanToUpdate && (
        <>
          <br />
          <FormControlLabel
            value="isDeleted"
            checked={newKanban.isDeleted}
            control={
              <Checkbox
                style={{
                  color: darkMode ? "#F0F2F5" : "#18191A"
                }}
              />
            }
            label={
              <p style={{ color: darkMode ? "#F0F2F5" : "#18191A" }}>Delete</p>
            }
            labelPlacement="start"
            onChange={(e, v) => setNewKanban({ ...newKanban, isDeleted: v })}
          />
        </>
      )}
      <br />
        <>
          <Autocomplete
            disableClearable
            multiple
            options={usersData
              .filter(el => !newKanban.invitedUsers?.some(em => em === el.id))
              .map(option => option.email)}
            value={usersData
              .filter(el => newKanban.invitedUsers?.some(em => em === el.id))
              .map(option => option.email)}
            freeSolo
            onChange={(e, newValue) =>
              setNewKanban({
                ...newKanban,
                invitedUsers: [
                  ...newValue.map(
                    em => usersData.find(user => user.email === em).id
                  )
                ]
              })
            }
            renderTags={(value, getTagProps) =>
              value.map((option, index) => (
                <Chip
                  variant="outlined"
                  label={
                    <p style={{ color: darkMode ? "#F0F2F5" : "#18191A" }}>
                      {option}
                    </p>
                  }
                  {...getTagProps({ index })}
                />
              ))
            }
            renderInput={params => (
              <TextField
                {...params}
                label={
                  <p style={{ color: darkMode ? "#F0F2F5" : "#18191A" }}>
                    Share with...
                  </p>
                }
              />
            )}
          />
        </>
      <br />
      <div className="servicesButtons">
        <Button
          variant="text"
          size="small"
          disabled={0 === newKanban.name?.length}
          onClick={() => {
            kanbanToUpdate ? handleUpdate() : handleCreate();
          }}
        >
          {kanbanToUpdate ? "Save" : "Create"}
        </Button>
        <Button variant="text" size="small" onClick={() => setShowKanbanCreateForm(false)}>
          Back
        </Button>
      </div>
    </>
  );
};

export default CreateNewKanban;
