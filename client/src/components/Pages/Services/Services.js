import React, { useEffect, useState } from "react";
import "./Services.scss";
import { Grid, Button } from "@material-ui/core";
import application from "../../../base";
import SettingsIcon from "@material-ui/icons/Settings";
import CreateNewCanvas from "./CreateNewCanvas";
import * as toastr from "toastr";
import { Divider } from "@material-ui/core";
import CreateNewKanban from "./CreateNewKanban";

const Services = props => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [myCanvases, setMyCanvases] = useState([]);
  const [publicCanvases, setPublicCanvases] = useState([]);
  const [isCreateCanvas, setIsCreateCanvas] = useState(false);
  const [invitedCanvases, setInvitedCanvases] = useState([]);
  const [renderCanvases, setRenderCanvases] = useState([]);
  const [canvasToUpdate, setCanvasToUpdate] = useState(null);
  const [showPublicCanvases, setShowPublicCanvases] = useState(true);
  const [myKanbans, setMyKanbans] = useState([]);
  const [invitedKanbans, setInvitedKanbans] = useState([]);
  const [showInvitedKanbans, setShowInvitedKanbans] = useState([]);
  const [renderKanbans, setRenderKanbans] = useState([]);
  const [showKanbanCreateForm, setShowKanbanCreateForm] = useState(false);
  const [kanbanToUpdate, setKanbanToUpdate] = useState(null);

  useEffect(() => {
    const fetchMyCanvases = async () => {
      await application
        .firestore()
        .collection("canvases")
        .where("createdBy", "==", user.user.uid)
        .where("isPublic", "==", false)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setMyCanvases(arr);
          },
          err => toastr.error(err)
        );
    };

    const fetchInvitedCanvases = async () => {
      await application
        .firestore()
        .collection("canvases")
        .where("invitedUsers", "array-contains", user?.user?.uid)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setInvitedCanvases(arr);
          },
          err => toastr.error(err)
        );
    };

    const fetchPublicCanvases = async () => {
      await application
        .firestore()
        .collection("canvases")
        .where("isPublic", "==", true)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setPublicCanvases(arr);
            setRenderCanvases(arr);
          },
          err => toastr.error(err)
        );
    };

    const fetchMyKanbans = async () => {
      await application
        .firestore()
        .collection("kanbans")
        .where("createdBy", "==", user.user.uid)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setMyKanbans(arr);
          },
          err => toastr.error(err)
        );
    };

    const fetchInvitedKanbans = async () => {
      await application
        .firestore()
        .collection("kanbans")
        .where("invitedUsers", "array-contains", user?.user?.uid)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setInvitedKanbans(arr);
            setRenderKanbans(arr);
          },
          err => toastr.error(err)
        );
    };

    fetchInvitedCanvases();
    fetchMyCanvases();
    fetchPublicCanvases();
    fetchMyKanbans();
    fetchInvitedKanbans();

    return async() => {
      await application
        .firestore()
        .collection("canvases")
        .where("isPublic", "==", true)
        .where("createdBy", "!=", user.user.uid)
        .onSnapshot(() => {});

        await application
        .firestore()
        .collection("kanbans")
        .where("isPublic", "==", true)
        .where("createdBy", "!=", user.user.uid)
        .onSnapshot(() => {});

        await application
        .firestore()
        .collection("canvases")
        .where("invitedUsers", "array-contains", user.user.uid)
        .onSnapshot(() => {});

        await application
        .firestore()
        .collection("canvases")
        .where("createdBy", "==", user.user.uid)
        .onSnapshot(() => {});
      
        await application
        .firestore()
        .collection("kanbans")
        .where("invitedUsers", "array-contains", user?.user?.uid)
        .onSnapshot(() => {});
    };
  }, []);

  const handleCreate = () => {
    setCanvasToUpdate(null);
    setIsCreateCanvas(true);
  };

  const handleShowPublicCanvases = () => {
    setIsCreateCanvas(false);
    setShowPublicCanvases(true);
    setRenderCanvases(publicCanvases);
  };

  const handleShowPrivateCanvases = () => {
    setIsCreateCanvas(false);
    setShowPublicCanvases(false);
    setRenderCanvases([...myCanvases, ...invitedCanvases]);
  };

  const handleCanvasUpdate = canvas => {
    setIsCreateCanvas(true);
  };

  const handleShowMyKanbans = () => {
    setShowInvitedKanbans(false);
    setRenderKanbans(myKanbans);
  };

  const handleShowInvitedKanbans = () => {
    setShowInvitedKanbans(true);
    setRenderKanbans(invitedKanbans);
  };

  return (
    <div className="services" style={{marginTop: '80px'}}>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
      >
        <Grid item xs={12} sm={6} xl={"auto"}>
          <div className="servicesListOuterBox">
            <h1>Drawing Boards</h1>
            <div className="servicesButtons">
              <Button onClick={handleCreate} className="actionButton">
                Create
              </Button>
              <Button onClick={handleShowPublicCanvases}>Public</Button>
              <Button onClick={handleShowPrivateCanvases}>Private</Button>
            </div>
            <div className="servicesListInnerBox">
              {!isCreateCanvas && (
                <>
                  <h2>{showPublicCanvases ? "Public" : "Private"}</h2>
                  <ul>
                    {renderCanvases.map((canvas, i) => {
                      return (
                        <div key={i} style={{ position: "relative" }}>
                          <li
                            onClick={() =>
                              props.history.push(`/canvases/${canvas.id}`)
                            }
                          >
                            <span>{i + 1}</span>
                            {canvas.name}
                          </li>
                          {user.user.uid === canvas.createdBy && (
                            <SettingsIcon
                              className="settingsIcon"
                              onClick={() => {
                                setCanvasToUpdate(canvas);
                                handleCanvasUpdate(canvasToUpdate);
                              }}
                              style={{
                                position: "absolute",
                                zIndex: 5,
                                top: 5,
                                right: 0,
                                padding: 5
                              }}
                            />
                          )}
                          <Divider className="servicesDivider" />
                        </div>
                      );
                    })}
                  </ul>
                </>
              )}
              {isCreateCanvas && (
                <CreateNewCanvas
                  setIsCreateCanvas={setIsCreateCanvas}
                  canvasToUpdate={canvasToUpdate}
                  setCanvasToUpdate={setCanvasToUpdate}
                />
              )}
            </div>
          </div>
        </Grid>
        <Divider orientation="vertical" flexItem className="servicesDivider" />
        <Grid item xs={12} sm={6} xl={"auto"}>
          <div className="servicesListOuterBox">
            <h1>Kanban Boards</h1>
            <div className="servicesButtons">
              <Button
                className="actionButton"
                onClick={() => setShowKanbanCreateForm(true)}
              >
                Create
              </Button>
              <Button onClick={handleShowInvitedKanbans}>Invited</Button>
              <Button onClick={handleShowMyKanbans}>Mine</Button>
            </div>
            <div className="servicesListInnerBox">
              {!showKanbanCreateForm && (
                <>
                  <h2>
                    {showInvitedKanbans ? "Invited" : "Mine"}
                  </h2>
                  <ul>
                    {renderKanbans.map((kanban, i) => {
                      return (
                        <div key={i} style={{ position: "relative" }}>
                          <li
                            onClick={() =>
                              props.history.push(`/kanbans/${kanban.id}`)
                            }
                          >
                            <span>{i + 1}</span>
                            {kanban.name}
                          </li>
                          {user.user.uid === kanban.createdBy && (
                            <SettingsIcon
                              className="settingsIcon"
                              onClick={() => {
                                setShowKanbanCreateForm(true)
                                setKanbanToUpdate(kanban);
                              }}
                              style={{
                                position: "absolute",
                                zIndex: 5,
                                top: 5,
                                right: 0,
                                padding: 5
                              }}
                            />
                          )}
                          <Divider className="servicesDivider" />
                        </div>
                      );
                    })}
                  </ul>
                </>
              )}
              {showKanbanCreateForm && (
                <CreateNewKanban
                  setShowKanbanCreateForm={setShowKanbanCreateForm}
                  kanbanToUpdate={kanbanToUpdate}
                  setKanbanToUpdate={setKanbanToUpdate}
                />
              )}
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Services;
