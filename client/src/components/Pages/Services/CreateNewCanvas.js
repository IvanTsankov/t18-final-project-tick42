import React, { useState, useEffect, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Chip from "@material-ui/core/Chip";
import application from "../../../base";
import DarkModeContext from "../../../providers/DarkModeContext";
import { Button } from "@material-ui/core";
import * as toastr from 'toastr';
import * as firebase from 'firebase';

const CreateNewCanvas = ({
  setIsCreateCanvas,
  canvasToUpdate,
  setCanvasToUpdate
}) => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [usersData, setusersData] = useState([]);
  const { darkMode } = useContext(DarkModeContext);
  const [newCanvas, setNewCanvas] = useState(
    canvasToUpdate
      ? canvasToUpdate
      : {
          createdBy: user.user.uid,
          time: new Date(),
          invitedUsers: canvasToUpdate
          ? usersData
              .filter(
                el => canvasToUpdate?.invitedUsers?.indexOf(el.id) !== -1
              )
              .map(el => el.email)
          : [],
          isPublic: true,
          name: "",
          isDeleted: false
        }
  );

  useEffect(() => {
    const fetchusersData = async () => {
      let arr = [];
      await application
        .firestore()
        .collection("users")
        .where("__name__", "!=", user.user.uid)
        .get()
        .then(data => {
          arr = data.docs.map(data => ({ ...data.data(), id: data.id }));
        }, err => toastr.error(err));
      setusersData(arr);
    };

    fetchusersData();

    return () => {
      setCanvasToUpdate(null);
      setNewCanvas({});
    }
  }, []);

  const handleBack = () => {
    setIsCreateCanvas(false);
    setCanvasToUpdate(null);
    setNewCanvas(null);
  };

  const handleCreate = async () => {
    await application
      .firestore()
      .collection("canvases")
      .add({...newCanvas, invitedUsers: newCanvas.invitedUsers})
      .then((el) => {
        sendNotification(newCanvas.invitedUsers, el.id)
      })
      .then(() => {
        setNewCanvas({ name: ''})
        setIsCreateCanvas(false); 
        setCanvasToUpdate(null);
      });

      
  };

  const handleUpdate = async () => {
    const newInvites = newCanvas.invitedUsers.filter(el => canvasToUpdate.invitedUsers.indexOf(el) === -1)
    await application
      .firestore()
      .collection("canvases")
      .doc(newCanvas.id)
      .set({
        createdBy: newCanvas.createdBy,
        time: newCanvas.time,
        invitedUsers: newCanvas.invitedUsers,
        isPublic: newCanvas.isPublic,
        name: newCanvas.name,
        isDeleted: newCanvas.isDeleted
      })
      .then((el) => {
        sendNotification(newInvites, canvasToUpdate.id)
        setCanvasToUpdate(null);
        setNewCanvas({name: ''})
        setIsCreateCanvas(false);
      });
  };
  
  const sendNotification = (userIds, boardId) => {
    userIds.forEach(async(id) => {
      await application
          .firestore()
          .collection("notifications")
          .doc(id)
          .set({
            notifications: firebase.firestore.FieldValue.arrayUnion({
            boardId: boardId,
            content: `${user.user.displayName || user.user.email.split('@')[0]} shared Drawing board ${newCanvas.name} with you!`,
            time: new Date(),
            isRead: false,

        })}, {merge: true})
    })
  }

  return (
    <>
      <h2>{0 < newCanvas.name?.length ? newCanvas.name : 'New Drawing board'}</h2>
      <TextField
        value={newCanvas.name}
        color="primary"
        id="name"
        label={<p style={{ color: darkMode ? '#F0F2F5' : '#18191A' }}>Name</p>}
        InputProps={{
          style: {color: darkMode ? '#F0F2F5' : '#18191A'}
        }}
        onChange={e => setNewCanvas({ ...newCanvas, name: e.target.value })}
      />
      <br />
      <br />
      <FormControlLabel
        value="isPublic"
        checked={!newCanvas.isPublic}
        control={
          <Checkbox
            style={{
              color: darkMode ? '#F0F2F5' : '#18191A'
            }}
          />
        }
        label={<p style={{ color: darkMode ? '#F0F2F5' : '#18191A' }}>Private</p>}
        labelPlacement="start"
        onChange={(e, v) => setNewCanvas({ ...newCanvas, isPublic: !v })}
      />
      {canvasToUpdate && (
        <>
          <br />
          <FormControlLabel
            value="isDeleted"
            checked={newCanvas.isDeleted}
            control={
              <Checkbox
                style={{
                  color: darkMode ? '#F0F2F5' : '#18191A'
                }}
              />
            }
            label={<p style={{ color: darkMode ? '#F0F2F5' : '#18191A'}}>Delete</p>}
            labelPlacement="start"
            onChange={(e, v) => setNewCanvas({ ...newCanvas, isDeleted: v })}
          />
        </>
      )}
      <br />
      {!newCanvas.isPublic && 
      <>
      <Autocomplete
        disableClearable
        multiple
        options={usersData.filter(el => !newCanvas.invitedUsers?.some(em => em === el.id)).map(option => option.email)}
        value={usersData.filter(el => newCanvas.invitedUsers?.some(em => em === el.id)).map(option => option.email)}
        freeSolo
        onChange={(e, newValue) =>
          setNewCanvas({
            ...newCanvas,
            invitedUsers: [
              ...newValue.map(em => usersData.find(user => user.email === em).id)
            ]
          })
        }
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              label={<p style={{ color: darkMode ? '#F0F2F5' : '#18191A' }}>{option}</p>}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={params => (
          <TextField
            {...params}
            label={<p style={{ color: darkMode ? '#F0F2F5' : '#18191A' }}
            >Share with...</p>}
          />
        )}
      />
      </>}
      <br />
      <div className="servicesButtons">
      <Button
        variant="text"
        size="small"
        disabled={0 === newCanvas.name?.length}
        onClick={() => {
          canvasToUpdate ? handleUpdate() : handleCreate();
        }}
      >
        {canvasToUpdate ? "Save" : "Create"}
      </Button>
      <Button variant="text" size="small" onClick={handleBack}>
        Back
      </Button>
      </div>
    </>
  );
};

export default CreateNewCanvas;
