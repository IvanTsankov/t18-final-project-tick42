import React, { useState, useEffect } from "react";
import application from "../../../base";
import * as toastr from "toastr";
import { Avatar } from "@material-ui/core";

const MouseSharing = ({ pageId }) => {
  const [mouses, setMouses] = useState([]);
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    const fetchDataRealTime = async () => {
      await application
        .firestore()
        .collection("mouses")
        .where("pageId", "==", pageId)
        .where("__name__", "!=", user.user.uid)
        .onSnapshot(
          querySnapshot => {
            const arr = [];
            querySnapshot.forEach(doc => {
              if (!doc.data().isDeleted) {
                arr.push({ ...doc.data(), id: doc.id });
              }
            });
            setMouses(arr);
          },
          err => toastr.error(err)
        );
    };

    fetchDataRealTime();
    return () =>
      application
        .firestore()
        .collection("mouses")
        .onSnapshot(() => {});
  }, []);

  return (
    <>
      {mouses.map((mouse, i) => {
        return (
          <div
            key={i}
            style={{
              position: "absolute",
              left: `${mouse.x}px`,
              top: `${mouse.y}px`
            }}
          >
            <img
            src="https://img.icons8.com/fluent/50/000000/cursor--v2.png"
            alt="mouse"
            style={{height: 20, display: 'inline-block', paddingRight: 20}}
             />
            <div style={{paddingLeft: 10, paddingBottom: 5}}>
            <Avatar alt="mouse" src={mouse.avatar} style={{width: 20, height: 20}}/>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default MouseSharing;
