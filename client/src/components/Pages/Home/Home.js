import React, { useState } from "react";
import "./Home.scss";
import laptopPNG from './../../../images/laptopPNG.png';
import tabletPNG from './../../../images/tabletPNG.png';
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
import { Grid } from "@material-ui/core";
import sass from './../../../images/sass.png';
import konva from './../../../images/konva.png';
import react from './../../../images/react.png';
import formik from './../../../images/formik.png';
import npm from './../../../images/npm.png';
import firebase from './../../../images/firebase.png';
import materialUI from './../../../images/materialUI.png';


const Home = () => {
    const [showLaptop, setShowLaptop] = useState(false);
    const [showTablet, setShowTablet] = useState(false);

    return (
            <Grid container direction="row" justify="center" alignItems="center" className="aboutWrapper">
                <div className="laptopWrapper" onWheel={() => setShowLaptop(true)} onMouseLeave={() => setShowLaptop(false)}>
                    <img alt="laptop" src={laptopPNG}/>
                    <div>
                    <div className="homeHeaders">
                        <Fade left opposite cascade collapse when={showLaptop}>
                            The Enterprise solution
                        </Fade>
                        <Fade left opposite cascade collapse when={showLaptop}>
                            for improving your team collaboration
                        </Fade>
                        
                    </div>
                    <div className="homeHeadersAfter">
                        <Fade right opposite cascade collapse when={showLaptop}>
                            Collaborate helps teams work more effectively and get more done.
                        </Fade>
                    </div>
                    </div>
                </div>
                <div className="tabletWrapper" onWheel={() => setShowTablet(true)} onMouseLeave={() => setShowTablet(false)}>
                    <div>
                    <div className="homeHeaders">
                        <Fade left opposite cascade collapse when={showTablet}>
                            Draw. Create. Share.
                        </Fade>
                        <Fade left opposite cascade collapse when={showTablet}>
                            Scale collaboration with confidence
                        </Fade>
                        
                    </div>
                    <div className="homeHeadersAfter">
                        <Fade right opposite cascade collapse when={showTablet}>
                            Our boards enable teams to organize
                        </Fade>
                        <Fade right opposite cascade collapse when={showTablet}>
                            and prioritize projects in a fun, flexible, and rewarding way.
                        </Fade>
                    </div>
                    </div>
                    <img alt="tablet" src={tabletPNG}/>
                </div>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    style={{height: 480}}
                >
                    <Grid item xs={"auto"} sm={"auto"} xl={"auto"}>
                        <div className="aboutBox">
                            <div className="aboutImgBox">
                                <img src={require('./../../../images/Vladilen.jpg')} alt="vladilen"/>
                            </div>
                            <div className="aboutContent">
                                <h2>
                                    Vladilen Panaiotov
                                    <br />
                                    <span>Junior developer</span>
                                </h2>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={"auto"} sm={"auto"} xl={"auto"}>
                        <div className="aboutBox item2">
                            <div className="aboutImgBox">
                                <img src={require('./../../../images/Ivan.jpg')} alt="ivan"/>
                            </div>
                            <div className="aboutContent">
                                <h2>
                                    Ivan Tsankov
                                    <br />
                                    <span>Junior developer</span>
                                </h2>
                            </div>
                        </div>
                    </Grid>
                </Grid> 
                <div className="footerImages">
                    <Zoom left>
                    <p><img alt="logo" src={konva}/></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo" src={react}/></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo" src={npm} style={{height: '40px', width: '60px'}} /></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo" src={formik}/></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo" src={firebase}/></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo"src={materialUI}/></p>
                    </Zoom>
                    <Zoom left>
                    <p><img alt="logo" src={sass}/></p>
                    </Zoom>
                </div>
            </Grid>
            
    );
};

export default Home;
