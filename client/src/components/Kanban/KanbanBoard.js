import React, { useState, useEffect, useContext } from "react";
import List from "./List/List";
import application from "./../../base";
import InputContainer from "./Input/InputContainer";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Button } from "@material-ui/core";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import ChatContext from "../../providers/ChatContext";
import * as toastr from "toastr";
import SideMenu from "./SideMenu/SideMenu";
import DarkModeContext from "../../providers/DarkModeContext";



const KanbanBoard = props => {
  const { darkMode } = useContext(DarkModeContext);
  const { setChat } = useContext(ChatContext);
  const [boardId, setboardId] = useState(props.location.pathname.substring(9));
  const [boardData, setBoardData] = useState([]);
  const user = JSON.parse(localStorage.getItem("user"));
  const [open, setOpen] = useState(false);
  const [backgroundUrl, setBackgroundUrl] = useState('');
  const [boardTitle, setBoardTitle] = useState('');
  
  const useStyle = makeStyles(theme => ({
    root: {
      display: "flex",
      minHeight: "100vh",
      borderRadius: "4px",
    }
  }));
  
  const classes = useStyle();
  useEffect(() => {
    const fetchBoard = async () => {
      await application
        .firestore()
        .collection("kanbans")
        .doc(boardId)
        .onSnapshot(data => {
         try {
          setChat({
            open: false,
            name: data.data().name,
            id: boardId,
            service: "kanbans"
          });
          setBackgroundUrl(data.data().backgroundUrl);
          setBoardTitle(data.data().name);
         } catch {
           props.history.push('/notfound')
         }
        },err => toastr.error(err) )
    };
    const fetchBoardLists = async () => {
      await application
        .firestore()
        .collection("lists")
        .where("boardId", "==", boardId)
        .onSnapshot(async querySnapshot => {
          const lists = [];
          querySnapshot.docs.map(list => {
            lists.push({ ...list.data(), documentId: list.id });
          });
          setBoardData(lists.sort((a, b) => a.index - b.index));
        });
    };

    fetchBoardLists();
    fetchBoard();

    return async () => {
      await application
        .firestore()
        .collection("lists")
        .where("boardId", "==", boardId)
        .onSnapshot(() => {});

      setChat({
        open: false,
        name: "General",
        id: "generalkj12bhk3b12k3h2J121"
      });
    };
  }, []);

  const onDragEnd = async ({ destination, source, draggableId, type }) => {
    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    if (type === "list") {
      const newBoardData = boardData.slice().map(list => ({ ...list }));
      const listDragged = newBoardData.find(list => list.id === draggableId);
      newBoardData.splice(source.index, 1);
      newBoardData.splice(destination.index, 0, listDragged);
      newBoardData.forEach((list, i) => (list.index = i));
      
      setBoardData(newBoardData);
      updateListIndexes(newBoardData);
      return;
    }

    const dataSource = boardData.find(list => list.id === source.droppableId);
    const sourceList = dataSource.cards.slice().map(el => ({ ...el }));
    const sourceDocumentId = dataSource.documentId;

    const draggedCard = sourceList.find(card => card.id === draggableId);
    sourceList.splice(source.index, 1);

    if (destination.droppableId === source.droppableId) {
      sourceList.splice(destination.index, 0, draggedCard);
      setBoardData(
        boardData.map(list => {
          if (list.documentId === sourceDocumentId) {
            return {
              ...list,
              cards: sourceList
            };
          }
          return list;
        })
      );
      updateListCards(sourceList, sourceDocumentId);
    } else {
      const dataDestination = boardData.find(
        list => list.id === destination.droppableId
      );
      const destinationList = dataDestination.cards
        .slice()
        .map(el => ({ ...el }));
      const destinationDocumentId = dataDestination.documentId;
      destinationList.splice(destination.index, 0, draggedCard);
      setBoardData(
        boardData.map(list => {
          if (list.documentId === sourceDocumentId) {
            return {
              ...list,
              cards: sourceList
            };
          } else if (list.documentId === destinationDocumentId) {
            return {
              ...list,
              cards: destinationList
            };
          }
          return list;
        })
      );
      updateListCards(sourceList, sourceDocumentId);
      updateListCards(destinationList, destinationDocumentId);
    }
  };

  const updateListCards = async (cardsArr, documentId) => {
    await application
      .firestore()
      .collection("lists")
      .doc(documentId)
      .update({ cards: cardsArr })
      .then(res => {})
      .catch(err => toastr.error(err));
  };

  const updateListIndexes = (newBoardData) => {
    newBoardData.forEach(async(list, i) => {
      await application
    .firestore()
    .collection('lists')
    .doc(list.documentId)
    .update({index: i})
    })
  }

  const handleMouseMove = async e => {
    await application
      .firestore()
      .collection("mouses")
      .doc(user.user.uid)
      .set({
        x: e.clientX,
        y: e.clientY,
        pageId: boardId,
        avatar: localStorage.getItem("imgUrl")
      });
  };
  return (
    <>
      <h1 style={{textAlign: 'center', fontSize: '40px', color: darkMode ? '#F0F2F5' : '#18191A'}}>{boardTitle}</h1>
            <Button style={{backgroundColor: '#3A3B3C', color: '#FFFFFF', marginBottom: '5px'}} onClick={() => setOpen(!open)}>Change Background</Button>
      <div className={classes.root} onMouseMove={handleMouseMove} style={{backgroundImage: `url(${backgroundUrl})`, backgroundSize: 'cover'}}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="app" type="list" direction="horizontal">
            {provided => (
              <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
              className={`${classes.root} ${backgroundUrl ? '' : 'kanbanContainer'}`}
              ref={provided.innerRef}
              {...provided.droppableProps}
              >
                {boardData.map((list, i) => {
                  return (
                    <List
                    key={list.id}
                    list={list}x
                    index={i}
                    />
                    );
                  })}
                <InputContainer
                  boardId={boardId}
                  board={Object.keys(boardData)}
                  />
                {provided.placeholder}
              </Grid>
            )}
          </Droppable>
        </DragDropContext>
        <SideMenu setOpen={setOpen} open={open} setBackgroundUrl={setBackgroundUrl} boardId={boardId}/>
      </div>
    </>
  );
};

export default KanbanBoard;
