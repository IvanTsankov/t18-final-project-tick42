import React, { useEffect, useState } from 'react';
import { Drawer, Typography, Divider, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Clear from '@material-ui/icons/Clear';
import { getImages } from './ImageApi';
import application from '../../../base';


const useStyle = makeStyles((theme) => ({
  drawerPaper: {
    width: '400px',
  },
  title: {
    fontSize: '1.2rem',
    fontWeight: 'bold',
  },
  titleContainer: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-around',
    marginBottom: theme.spacing(2),
  },
  menuContainer: {
    display: 'flex',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  menu: {
    width: '45%',
    height: '90px',
    background: 'blue',
    display: 'flex',
    alignItems: 'flex-end',
    borderRadius: '8px',
    marginTop: theme.spacing(2),
  },
  ref: {
    fontSize: '1rem',
    color: '#fff',
  },
}));
const SideMenu = ({ setBackgroundUrl, setOpen, open, boardId }) => {
  const classes = useStyle();
  const [photos, setPhotos] = useState([]);
  const readImge = async () => {
    setPhotos(await getImages());
  };
  useEffect(() => {
    readImge();
  }, []);

  const handleSetPhoto = async(photo) => {
    setBackgroundUrl(photo);
    await application
    .firestore()
    .collection('kanbans')
    .doc(boardId)
    .update({backgroundUrl: photo})
  }
  return (
    <Drawer
      open={open}
      onClose={() => setOpen(!open)}
      anchor="left"
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.titleContainer}>
        <Typography className={classes.title}>Change Background</Typography>
        <Clear onClick={() => setOpen(!open)} style={{cursor: 'pointer'}} />
      </div>
      <Divider />

      <div className={classes.menuContainer}>
        <div
          className={classes.menu}
          style={{
            backgroundImage: `url(https://images.pexels.com/photos/755726/pexels-photo-755726.jpeg?cs=srgb&dl=astronomy-astrophotography-clouds-colors-755726.jpg&fm=jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            cursor: 'pointer'
          }}
        >
          <span style={{color: 'white'}}>photo</span>
        </div>
        <div
          className={classes.menu}
          style={{
            backgroundImage: `url(https://images.pexels.com/photos/226589/pexels-photo-226589.jpeg?cs=srgb&dl=closeup-photo-of-multi-color-stick-226589.jpg&fm=jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            cursor: 'pointer'
          }}
        >
          <span style={{color: 'white'}}>color</span>
        </div>
      </div>
      <div className={classes.menuContainer}>
        {photos.map((photo, index) => (
          <div
            className={classes.menu}
            style={{
              backgroundImage: `url(${photo.thumb})`,
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
              cursor: 'pointer'
            }}
            onClick={() => handleSetPhoto(photo.full)}
            key={index}
          >
            <span>
              <Link
                href={photo.user.link}
                target="_blank"
                className={classes.ref}
              >
                {photo.user.username}
              </Link>
            </span>
          </div>
        ))}
      </div>
    </Drawer>
  );
}

export default SideMenu;