import React, { useContext, useState } from "react";
import { Paper, InputBase, Button, IconButton } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import { makeStyles, fade } from "@material-ui/core/styles";
import application from "../../../base";
import DarkModeContext from "../../../providers/DarkModeContext";
import { v4 as uuidv4 } from "uuid";
import * as firebase from "firebase";
import * as toastr from 'toastr';

const InputCard = ({ setOpen, listId, boardId }) => {
  const [newCard, setNewCard] = useState({});
  const { darkMode } = useContext(DarkModeContext);

  const useStyle = makeStyles(theme => ({
    card: {
      margin: theme.spacing(0, 1, 1, 1),
      paddingBottom: theme.spacing(4)
    },
    input: {
      margin: theme.spacing(1)
    },
    btnConfirm: {
      background: fade("#21b573", 0.7),
      color: "#F0F2F5",
      "&:hover": {
        background: fade("#21b573", 1)
      }
    },
    confirm: {
      margin: theme.spacing(0, 1, 1, 1)
    }
  }));
  const classes = useStyle();

  const addCardToList = async () => {
    setOpen(false);
    if (listId) {
      await application
        .firestore()
        .collection("lists")
        .doc(listId)
        .set(
          {
            cards: firebase.firestore.FieldValue.arrayUnion({
              ...newCard,
              assignedUsers: [],
              id: uuidv4(),
              dueDate: null,
              priority: 0,
              description: '',
              comments: [],
              checkList: [],
              isDueDateNotificationSent: false,
            })
          },
          { merge: true }
        );
    }
    setNewCard({title: ''})
  };

  const addNewList = async () => {
    delete newCard.listId;
    await application
      .firestore()
      .collection("lists")
      .add({
        ...newCard,
        time: new Date(),
        cards: [],
        id: uuidv4(),
        boardId: boardId
      })
      .then(async res => {
        await application
          .firestore()
          .collection("lists")
          .doc(res.id)
          .set({ documentId: res.id }, { merge: true });
        setNewCard({ title: "" });
      })
      .catch(err => toastr.error(err));
  };

  const handleChange = e => {
    setNewCard({ ...newCard, title: e.target.value });
  };

  return (
    <div>
      <div>
        <Paper className={classes.card}>
          <InputBase
            multiline
            fullWidth
            value={newCard?.title}
            inputProps={{
              className: classes.input
            }}
            onChange={handleChange}
            placeholder={
              listId
                ? "Enter a title of this card.."
                : "Enter a title of this list.."
            }
          />
        </Paper>
      </div>
      <div className={classes.confirm}>
        <Button
          className={classes.btnConfirm}
          onClick={listId ? addCardToList : addNewList}
        >
          {listId ? "Add Card" : "Add List"}
        </Button>
        <IconButton
          onClick={() => setOpen(false)}
          style={{ color: darkMode ? "#F0F2F5" : "#18191A" }}
        >
          <ClearIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default InputCard;
