import React, { useState } from 'react';
import { Paper, Typography, Collapse } from '@material-ui/core';
import { makeStyles, fade } from '@material-ui/core/styles';
import InputCard from './InputCard';

const useStyle = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    width: '300px',
  },
  addCard: {
    padding: theme.spacing(1, 1, 1, 2),
    margin: theme.spacing(1),
    background: '#EBECF0',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: fade('#21b573', 1),
    },
  },
}));

const InputContainer = ({ listId, boardId, listCards, board }) => {
  const classes = useStyle();
  const [open, setOpen] = useState(false);

  return (
    <div className={classes.root}>
      <Collapse in={open}>
        <InputCard setOpen={setOpen} listId={listId} boardId={boardId} listCards={listCards} board={board}/>
      </Collapse>
      <Collapse in={!open}>
        <Paper
          className={classes.addCard}
          elevation={0}
          onClick={() => setOpen(!open)}
        >
          <Typography>{`Add a ${listId ? 'Card' : 'List'}`}</Typography>
        </Paper>
      </Collapse>
    </div>
  );
}

export default InputContainer;
