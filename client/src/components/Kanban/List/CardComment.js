import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography
} from "@material-ui/core";
import DarkModeContext from "../../../providers/DarkModeContext";
import { Button } from "@material-ui/core";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';



const CardComment = ({ comment, card, updateCards, allCards }) => {
    const {darkMode} = useContext(DarkModeContext);
    const user = JSON.parse(localStorage.getItem('user'));
    const useStyles = makeStyles(theme => ({
        root: {
          width: "100%",
          backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
          maxHeight: '150px',
          overflowY: 'auto',
          overflowX: 'hidden'
        },
        fonts: {
          fontWeight: "bold",
          color: darkMode ? "#E5E5E5" : "#242526"
        },
        inline: {
          display: "inline",
          color: darkMode ? "#969AA3" : "#3A3B3C"
        }
      }));
  const classes = useStyles();

  const handleDelete = (e) => {
    const updatedComments = card.comments.slice().map(el => {
      if (el.id === comment.id) {
        return {
          ...el,
          isDeleted: !el.isDeleted,
        }
      }
      return el;
    });
    const updatedCards = allCards.slice().map(el => {
      if(el.id === card.id) {
        return {
          ...el,
          comments: updatedComments
        }
      }
      return el;
    })
    updateCards(updatedCards)
  }

  return (
          <React.Fragment key={comment.id}>
            <ListItem key={comment.id} alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt="avatar" src={comment.avatar}/>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <Typography className={classes.fonts}>
                    {comment.author}
                  </Typography>
                }
                secondary={
                  <>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      {comment.comment}
                    </Typography>
                  </>
                }
              />
            {user.user.uid === comment.authorId ? <Button onClick={handleDelete}><DeleteForeverIcon style={{ color: "#969AA3" }}/></Button> : null}
            </ListItem>
            <Divider />
          </React.Fragment>
  );
};

export default CardComment;