import React, { useContext, useState, useEffect, createRef, useCallback } from "react";
import {
  Paper,
  TextField,
  MenuItem,
  Select,
  Button,
  Chip
} from "@material-ui/core";
import DarkModeContext from "./../../../providers/DarkModeContext";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import application from "../../../base";
import * as toastr from "toastr";
import { Avatar, Divider } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";
import CardProgressBar from "./CardProgressBar";
import { Draggable } from "react-beautiful-dnd";
import "./Card.scss";
import Title from "./Title";
import pencilSVG from "./../../../images/pencil.svg";
import checkboxSVG from "./../../../images/checkbox.svg";
import titleSVG from "./../../../images/title.svg";
import List from "@material-ui/core/List";
import CardCheckboxItem from "./CardCheckboxItem";
import { KeyboardDatePicker } from "@material-ui/pickers";
import AddIcon from "@material-ui/icons/Add";
import commentSVG from "./../../../images/comment.svg";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CheckIcon from '@material-ui/icons/Check';
import CardComment from "./CardComment";
import { v4 as uuidv4 } from "uuid";
import * as firebase from 'firebase';

const colorPriority = ["#35FF69", "#ffd500", "#f94144"];

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Card = ({ card, index, list }) => {
  const user = JSON.parse(localStorage.getItem('user'))
  const { darkMode } = useContext(DarkModeContext);
  const [usersData, setUsersData] = useState([]);
  const [hideDone, setHideDone] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [checkboxAdd, setCheckboxAdd] = useState(false);
  const [checkboxValue, setCheckboxValue] = useState("");
  const [cardDescription, setCardDescription] = useState(card.description);
  const [descriptionDisabled, setDescriptionDisabled] = useState(true);
  const [selectedDate, setSelectedDate] = useState(
    moment(card.dueDate?.toDate())
      .format("DD/MM/YYYY")
      .split("/")
      .reverse()
      .join("-")
  );
  const refDescription = createRef();
  const [priority, setPrority] = useState(card.priority);
  const [showAddUsers, setShowAddUsers] = useState(false);
  const [assignedUsers, setAssignedUsers] = useState([]);
  const [commentAdd, setCommentAdd] = useState(false);
  const [commentValue, setCommentValue] = useState("");
  const dateDifference = moment(moment(card.dueDate?.toDate())
  .format("DD/MM/YYYY")
  .split("/")
  .reverse()
  .join("-")).diff(moment(new Date())
  .format("DD/MM/YYYY")
  .split("/")
  .reverse()
  .join("-"), 'day')

  const useStyle = makeStyles(theme => ({
    card: {
      padding: theme.spacing(1, 1, 1, 2),
      margin: theme.spacing(1),
      backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
      borderRadius: "4px",
      border: `1px solid ${darkMode ? "#3A3B3C" : "#E5E5E5"}`,
      color: `${darkMode ? "#E5E5E5" : "#3A3B3C"}`,
      cursor: "pointer"
    },
    cardHeader: {
      display: "flex",
      justifyContent: "space-between",
      paddingBottom: 5
    },
    priorityBar: {
      height: 8,
      width: 50,
      backgroundColor: colorPriority[card.priority],
      borderRadius: 4,
      color: darkMode ? "#E5E5E5" : "#3A3B3C"
    },
    dueDate: {
      backgroundColor: 10 < dateDifference ? "#21b573" : 3 < dateDifference ? '#FCE836' : '#C1292E',
      display: "flex",
      color: "#FFFFFF",
      borderRadius: 4,
      justifyContent: "center",
      width: 85,
      fontSize: 12
    },

    cardFooter: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "flex-end",
      paddingTop: 10
    },
    assignedUsers: {
      display: "flex",
      flexWrap: "wrap"
    },
    progressBar: {
      display: "flex"
    },
    list: {
      width: '100%',
      maxHeight: '150px',
      overflowY: 'auto',
      overflowX: 'hidden'
    },
    commentsList: {
      width: "100%",
      backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
      maxHeight: '150px',
      overflowY: 'auto',
      overflowX: 'hidden'
    }
  }));

  const classes = useStyle();

  const sendNotifications = (userIds, kanbanId, dueDateNotification) => {
    if(dueDateNotification) {
      userIds.forEach(async(el) => {
        await application
            .firestore()
            .collection("notifications")
            .doc(el.id)
            .set({
              notifications: firebase.firestore.FieldValue.arrayUnion({
              kanbanId: kanbanId,
              content: `${card.title} due date is in ${dueDateNotification} days!`,
              time: new Date(),
              isRead: false,
  
          })}, {merge: true})
      })
      return;
    }
    userIds.forEach(async(el) => {
      await application
          .firestore()
          .collection("notifications")
          .doc(el.id)
          .set({
            notifications: firebase.firestore.FieldValue.arrayUnion({
            kanbanId: kanbanId,
            content: `${user.user.displayName || user.user.email.split('@')[0]} assigned a Kanban card to you!`,
            time: new Date(),
            isRead: false,

        })}, {merge: true})
    })
  }

  useEffect(() => {
    const fetchUsersData = async () => {
      await application
        .firestore()
        .collection("users")
        .onSnapshot(
          querySnapshot => {
            const users = querySnapshot.docs.map(user => ({
              ...user.data(),
              id: user.id
            }));
            setUsersData(users);
            setAssignedUsers(
              users.filter(user => card.assignedUsers.some(el => el.id === user.id))
            );
          },
          err => {
            toastr.error(`Encountered error: ${err}`);
          }
        );
    };
    const dateDiff = moment(moment(card.dueDate?.toDate())
  .format("DD/MM/YYYY")
  .split("/")
  .reverse()
  .join("-")).diff(moment(new Date())
  .format("DD/MM/YYYY")
  .split("/")
  .reverse()
  .join("-"), 'day');
  if(dateDiff > 0 && dateDiff <= 3) {
    if(card.isDueDateNotificationSent === false) {
      const updatedCards = list.cards.slice().map(el => {
        if (el.id === card.id) {
          return {
            ...el,
            isDueDateNotificationSent: true
          }
        }
        return el;
      })
      sendNotifications(card?.assignedUsers, list.boardId, dateDiff);
      updateCards(updatedCards);
    }
  }


    fetchUsersData();

    return async () => {
      await application
        .firestore()
        .collection("users")
        .onSnapshot(() => {});
    };
  }, []);

  const handleCheckboxValue = event => {
    setCheckboxValue(event.target.value);
  };

  const handleCommentValue = event => {
    setCommentValue(event.target.value);
  };

  
  const sendComment = async value => {
    if(commentValue === '') return;

    const updatedCards = list.cards.slice().map(el => {
      if (el.id === card.id) {
        return {
          ...el,
          comments: [...el.comments, {comment: value, isDeleted: false, time: new Date(), id: uuidv4(), author: user.user.displayName, authorId: user.user.uid,avatar: localStorage.getItem('imgUrl')}]
        }
      }
      return el;
    })
    setCommentAdd(false);
    updateCards(updatedCards);
  };

  const handleDateChange = async date => {
    if (
      moment(date.toDate()).format("DD/MM/YYYY") ===
      moment(selectedDate).format("DD/MM/YYYY")
    ) {
      return;
    }
    const updatedCards = list.cards.slice().map(el => {
      if (el.id === card.id) {
        return {
          ...el,
          dueDate: moment(date, "YYYY-MM-DD").toDate()
        };
      }
      return el;
    });

    setSelectedDate(date);
    updateCards(updatedCards);
  };

  const sendText = async value => {
    if (value === card.description) {
      setDescriptionDisabled(true);
      return;
    }
    const updatedCards = list.cards.slice().map(el => {
      if (el.id === card.id) {
        return {
          ...el,
          description: value
        };
      }
      return el;
    });
    setDescriptionDisabled(true);
    updateCards(updatedCards);
  };

  const updateCards = async (updatedCards) => {
    await application
      .firestore()
      .collection("lists")
      .doc(list.documentId)
      .update({ cards: updatedCards })
      .then(res => toastr.success(`Card updated successfully!`))
      .catch(err => toastr.error(err));
  };

  const handlePriorityChange = e => {
    const updatedCards = list.cards.slice().map(el => {
      if (el.id === card.id) {
        return {
          ...el,
          priority: e.target.value
        };
      }
      return el;
    });
    setPrority(e.target.value);
    updateCards(updatedCards);
  };


  const handleAddAssignees = () => {
    const newAssignees = assignedUsers.filter((user) => {
      return !card.assignedUsers.some((el) => el.id === user.id);
    })
    const updatedCards = list.cards.slice().map(el => {
      if (card.id === el.id) {
        return {
          ...el,
          assignedUsers 
        }
      }
      return el;
    })
    sendNotifications(newAssignees, list.boardId, false);
    updateCards(updatedCards);
    setShowAddUsers(false);
  }

  const handleAddCheckbox = () => {
    const updatedCards = list.cards.slice().map(el => {
      if (el.id === card.id) {
        return {
          ...el,
          checkList: [...el.checkList, {title: checkboxValue, isDone: false, id: uuidv4(), isDeleted: false}]
        }
      }
      return el;
    })
    updateCards(updatedCards);
    setCheckboxAdd(false);
    setCheckboxValue('');
  }

  const handleHideDone = (e) => {
    e.preventDefault()
    setHideDone(!hideDone)
  }

  const handleDialogClose = () => {
    setHideDone(false);
    setCheckboxAdd(false);
    setCheckboxValue('');
    setCardDescription(card.description);
    setDescriptionDisabled(true);
    setShowAddUsers(false);
    setCommentAdd(false);
    setCommentValue('');
    setOpenDialog(false);
  }

  return (
    <div>
      <Draggable draggableId={card.id} index={index}>
        {provided => (
          <div
            ref={provided.innerRef}
            {...provided.dragHandleProps}
            {...provided.draggableProps}
          >
            <Paper className={classes.card} onClick={() => setOpenDialog(true)}>
              <div className={classes.cardHeader}>
                <div className={classes.priorityBar} />
                <div className={classes.dueDate}>
                  {moment(card.dueDate?.toDate()).format("DD/MM/YYYY")}
                </div>
              </div>
              <h2>{card.title}</h2>
              <div className={classes.cardFooter}>
                <div className="iconsWrapper">
                  <div style={{ display: "flex" }}>
                    <img alt="comment" src={commentSVG} />
                    <span>{card.comments.filter(el => !el.isDeleted).length}</span>
                  </div>
                  <div>
                    <img
                      src={checkboxSVG}
                      alt="checkbox"
                      style={{ height: "13px", width: "13px" }}
                    />
                  <span>{`${card.checkList.filter(el => el.isDone).filter(el => !el.isDeleted).length}/${card.checkList.filter(el => !el.isDeleted).length}`}</span>
                  </div>
                </div>
                <div className={classes.assignedUsers}>
                  {assignedUsers.map((user, i) => {
                    return (
                      <Avatar
                        alt="photo"
                        key={i}
                        src={user.avatar}
                        style={{ width: 20, height: 20, marginLeft: 5 }}
                      />
                    );
                  })}
                </div>
              </div>
            </Paper>
          </div>
        )}
      </Draggable>
      <Dialog
        open={openDialog}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleDialogClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        PaperProps={{
          style: {
            backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
            color: darkMode ? "#F0F2F5" : "#18191A",
            height: "auto",
            width: "600px"
          }
        }}
      >
        <div className="dialogOuterContainer">
          <div className="dialogTitleContainer">
            <img
            alt="title"
              src={titleSVG}
              style={{
                height: "auto",
                width: "25px",
                position: "absolute",
                left: "12px",
                top: "35px"
              }}
            />
            <Title
              card={card}
              type="card"
              className="dialogTitle"
              list={list}
            />
            <div style={{ marginLeft: "8px" }}>
              In list <span>{list.title}</span>
            </div>
          </div>
          <DialogContent className="dialogInnerContainer">
            <div className="dialogTopItems">
              <div className="dialogMembers">
                <span className="dialogMembersTitle">
                  Members
                  <Button onClick={() => setShowAddUsers(!showAddUsers)} style={{margin: '0px 0px 0px 10px', padding: '0px', minWidth: '20px', height: '20px'}}>
                    <AddIcon
                      style={{ color: "#969AA3" }}
                    />
                  </Button>
                </span>
                <div className="dialogMembersItems">
                  {showAddUsers ? (
                    <>
                      <Autocomplete
                        disableClearable
                        multiple
                        options={usersData
                          .filter(
                            el => !assignedUsers?.some(em => em === el.id)
                          )
                          .map(option => option.email)}
                        value={assignedUsers.map(option => option.email)}
                        freeSolo
                        style={{ width: 200 }}
                        onChange={(e, newValue) =>
                          setAssignedUsers([
                            ...newValue.map(em =>
                              usersData.find(user => user.email === em)
                            )
                          ])
                        }
                        renderTags={(value, getTagProps) =>
                          value.map((option, index) => (
                            <Chip
                              variant="outlined"
                              style={{ backgroundColor: "#21b573" }}
                              label={
                                <p
                                  style={{
                                    color: darkMode ? "#F0F2F5" : "#18191A"
                                  }}
                                >
                                  {option}
                                </p>
                              }
                              {...getTagProps({ index })}
                            />
                          ))
                        }
                        renderInput={params => (
                          <TextField
                            {...params}
                            label={
                              <p
                                style={{
                                  color: "#21b573"
                                }}
                              >
                                Share with...
                              </p>
                            }
                            autoFocus
                          />
                        )}
                      />
                      <br />
                      <Button
                      onClick={handleAddAssignees}
                      >
                        <CheckIcon
                          style={{ color: "#21b573" }}
                        />
                      </Button>
                    </>
                  ) : (
                    assignedUsers.map((user, i) => {
                      return (
                        <Avatar
                          alt="photo"
                          key={i}
                          src={user.avatar}
                          style={{
                            width: 20,
                            height: 20,
                            marginLeft: 5,
                            marginBottom: 30
                          }}
                        />
                      );
                    })
                  )}
                </div>
              </div>
              <div className="dialogPriority">
                <div className="dialogPriorityTitle">Priority</div>
                <div className="dialogPriorityLine">
                <Select
                  value={priority}
                  onChange={handlePriorityChange}
                  style={{backgroundColor: darkMode ? '#18191A' : '#F0F2F5'}}
                >
                  <MenuItem value="0" style={{backgroundColor: darkMode ? '#969AA3' :  '#F0F2F5'}}>
                  <div className={classes.priorityBar} style={{backgroundColor: "#35FF69"}} />
                  </MenuItem>
                  <MenuItem value="1" style={{backgroundColor: darkMode ? '#969AA3' :  '#F0F2F5'}}>
                  <div className={classes.priorityBar} style={{backgroundColor: "#ffd500" }} />
                  </MenuItem>
                  <MenuItem value="2" style={{backgroundColor: darkMode ? '#969AA3' :  '#F0F2F5'}}>
                  <div className={classes.priorityBar} style={{backgroundColor: "#f94144"}} />
                  </MenuItem>
                  </Select>
                </div>
              </div>
              <div className="dialogDueDate">
                <KeyboardDatePicker
                  label="Due Date"
                  format="DD/MM/YYYY"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                  inputProps={{
                    style: {
                      color: darkMode ? "#E5E5E5" : "#18191A",
                      pointerEvents: 'none',
                      caretColor: 'transparent'
                    }
                  }}
                  InputLabelProps={{
                    style: {
                      color: darkMode ? "#F0F2F5" : "#18191A"
                    }
                  }}
                />
              </div>
            </div>
            <div className="dialogDescription">
              <div className="dialogDescriptionItem">
                <img
                alt="pencil"
                  src={pencilSVG}
                  style={{
                    height: "auto",
                    width: "20px",
                    position: "absolute",
                    left: "15px"
                  }}
                />
                <p style={{textDecoration: 'underline'}}
                  onClick={() => {
                    setDescriptionDisabled(!descriptionDisabled);
                  }}
                >
                  Edit
                </p>
                {descriptionDisabled ? null : (
                  <button
                    onClick={() => sendText(cardDescription)}
                    className="cardSendButton"
                  >
                    Send
                  </button>
                )}
                <TextField
                  id="standard-full-width"
                  label="Description"
                  placeholder="Enter a description"
                  multiline
                  rowsMax={2}
                  value={cardDescription}
                  onChange={event => setCardDescription(event.target.value)}
                  disabled={descriptionDisabled}
                  inputRef={refDescription}
                  inputProps={{
                    style: {
                      color: darkMode ? "#E5E5E5" : "#18191A",
                      marginTop: '5px'
                    }
                  }}
                  InputLabelProps={{
                    shrink: true,
                    style: {
                      color: darkMode ? "#F0F2F5" : "#18191A"
                    }
                  }}
                />
              </div>
            </div>
            <Divider className="cardDivider" />
            <div className="dialogCheckbox">
              <img
              alt="checkbox"
                src={checkboxSVG}
                style={{
                  height: "auto",
                  width: "20px",
                  position: "absolute",
                  left: "15px"
                }}
              />
              <div className="dialogCheckboxTitle">Checkbox items</div>
              <div className="dialogCheckboxItems">
                <p style={{textDecoration: 'underline'}} onClick={() => setCheckboxAdd(!checkboxAdd)}>
                  Add
                </p>
                <p style={{textDecoration: 'underline'}} onClick={handleHideDone}>{hideDone ? 'Show all items' : 'Hide completed items'}</p>
              </div>
            </div>
            <div className="dialogProgressBar">
              <CardProgressBar fullwidth={true}  checkList={card.checkList.filter(el => !el.isDeleted)}/>
            </div>
            {checkboxAdd ? (
              <div className="checkboxAddWrapper">
                <TextField
                  id="standard-full-width"
                  placeholder="Enter an item name"
                  multiline
                  rowsMax={2}
                  value={checkboxValue}
                  onChange={handleCheckboxValue}
                  inputProps={{
                    style: {
                      color: darkMode ? "#F0F2F5" : "#18191A"
                    }
                  }}
                  InputLabelProps={{
                    shrink: true,
                    style: {
                      color: darkMode ? "#F0F2F5" : "#18191A"
                    }
                  }}
                />
                <button
                  onClick={handleAddCheckbox}
                  className="cardSendButton"
                >
                  Send
                </button>
              </div>
            ) : (
              <div className="dialogCheckListItems" >
                <List className={classes.list} >
                  {card.checkList.filter(el => !el.isDeleted).map((item, index) => {
                    if (hideDone && item.isDone) {
                      return;
                    }
                    return (
                      <CardCheckboxItem item={item} list={list} card={card} key={index} updateCards={updateCards}/>
                    )
                  })}
                </List>
              </div>
            )}
            <Divider className="cardDivider" />
            <div className="cardComments">
              <img
              alt="comment"
                src={commentSVG}
                style={{
                  height: "auto",
                  width: "20px",
                  position: "absolute",
                  left: "15px"
                }}
              />
              <div className="dialogCheckboxTitle" style={{marginBottom: '5px', display: 'flex'}}>Comments
                <p style={{textDecoration: 'underline', marginLeft: '15px'}} onClick={() => setCommentAdd(!commentAdd)} >
                  Add
                </p>
              </div>
            {commentAdd ? (
              <div className="checkboxAddWrapper">
                <TextField
                  id="standard-full-width"
                  placeholder="Enter a comment"
                  multiline
                  rowsMax={2}
                  value={commentValue}
                  onChange={handleCommentValue}
                  inputProps={{
                    style: {
                      color: darkMode ? "#F0F2F5" : "#18191A"
                    }
                  }}
                  InputLabelProps={{
                    shrink: true,
                    style: {
                      color: darkMode ? "#969AA3" : "#3A3B3C"
                    }
                  }}
                />
                <button
                  onClick={() => sendComment(commentValue)}
                  className="cardSendButton"
                >
                  Send
                </button>
              </div>
            )
            
            : <List className={classes.commentsList}>
              {card.comments.filter(el => !el.isDeleted).map((comment, index) => <CardComment comment={comment} key={comment.id} card={card} updateCards={updateCards} allCards={list.cards} />)}
            </List>}
            </div>
          </DialogContent>
        </div>
      </Dialog>
    </div>
  );
};

export default Card;
