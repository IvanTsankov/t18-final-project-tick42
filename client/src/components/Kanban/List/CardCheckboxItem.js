import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import { Button } from "@material-ui/core";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const CardCheckboxItem = ({ item, list, labelId, card, updateCards }) => {

  const handleCheckBoxIsDone = () => {
    const updatedCheckboxes = card.checkList.slice().map(el => {
      if (el.id === item.id) {
        return {
          ...el,
          isDone: !el.isDone,
        }
      }
      return el;
    });
    const updatedCards = list.cards.slice().map(el => {
      if(el.id === card.id) {
        return {
          ...el,
          checkList: updatedCheckboxes
        }
      }
      return el;
    })
    updateCards(updatedCards)
    }

  const handleDelete = (e) => {
    const updatedCheckboxes = card.checkList.slice().map(el => {
      if (el.id === item.id) {
        return {
          ...el,
          isDeleted: !el.isDeleted,
        }
      }
      return el;
    });

    const updatedCards = list.cards.slice().map(el => {
      if(el.id === card.id) {
        return {
          ...el,
          checkList: updatedCheckboxes
        }
      }
      return el;
    })
    updateCards(updatedCards)
  }
  
  return (
    <>
    <ListItem
      key={item}
      role={undefined}
      dense
    >
      <ListItemIcon>
        <Checkbox
          edge="start"
          checked={item.isDone}
          tabIndex={-1}
          disableRipple
          inputProps={{ "aria-labelledby": labelId }}
          className="cardCheckbox"
          onClick={handleCheckBoxIsDone}
        />
      </ListItemIcon>
      <ListItemText id={labelId} primary={item.title} />
      <Button onClick={handleDelete}><DeleteForeverIcon style={{ color: "#969AA3" }}/></Button>
    </ListItem>
    <hr style={{color: "#969AA3", width: '90%'}}/>
    </>
  );
};

export default CardCheckboxItem;
