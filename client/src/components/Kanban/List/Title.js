import React, { useContext, useState } from "react";
import { Typography, InputBase } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import application from "../../../base";
import * as toastr from "toastr";
import DarkModeContext from "../../../providers/DarkModeContext";

const Title = ({ list, card }) => {
  const { darkMode } = useContext(DarkModeContext);
  const [open, setOpen] = useState(false);
  const [newTitle, setNewTitle] = useState(card ? card?.title : list?.title);

  const useStyle = makeStyles(theme => ({
    editableTitleContainer: {
      margin: theme.spacing(1),
      display: "flex"
    },
    editableTitle: {
      flexGrow: 1,
      fontSize: "1.2rem",
      fontWeight: "bold",
      color: darkMode ? "#E5E5E5" : "#3A3B3C"
    },
    input: {
      fontSize: "1.2rem",
      fontWeight: "bold",
      borderRadius: "4px",
      margin: theme.spacing(1),
      "&:focus": {
        background: darkMode ? "#3A3B3C" : "#E5E5E5",
        color: darkMode ? "#E5E5E5" : "#3A3B3C"
      }
    }
  }));

  const classes = useStyle();

  const handleBlur = async () => {
    setOpen(!open);
    if (card) {
      const updatedCards = list.cards.slice().map(el => {
        if (el.id === card.id) {
          return {
            ...el,
            title: newTitle
          };
        }
        return el;
      });
      await application
        .firestore()
        .collection("lists")
        .doc(list.documentId)
        .update({ cards: updatedCards })
        .then(res => toastr.success(`Title changed successfully!`))
        .catch(err => toastr.error(err));
      return;
    }
    await application
      .firestore()
      .collection("lists")
      .doc(list.documentId)
      .update({
        title: newTitle
      })
      .then(res => toastr.success(`Title changed successfully!`))
      .catch(err => toastr.error(err));
  };

  const handleEnter = async e => {
    if (e.key !== "Enter") return;
    handleBlur();
  };

  return (
    <div>
      {open ? (
        <div>
          <InputBase
            autoFocus
            value={newTitle}
            inputProps={{
              className: classes.input
            }}
            fullWidth
            onBlur={handleBlur}
            onChange={e => setNewTitle(e.target.value)}
            onKeyPress={handleEnter}
          />
        </div>
      ) : (
        <div className={classes.editableTitleContainer}>
          <Typography
            onClick={() => setOpen(!open)}
            className={classes.editableTitle}
          >
            {newTitle}
          </Typography>
        </div>
        )
      }
    </div>
  );
};

export default Title;
