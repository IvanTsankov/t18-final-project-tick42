import React, { useContext } from "react";
import { Paper, CssBaseline, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Title from "./Title";
import Card from "./Card";
import InputContainer from "../Input/InputContainer";
import DarkModeContext from "./../../../providers/DarkModeContext";
import { Droppable, Draggable } from "react-beautiful-dnd";

const List = ({ list, index }) => {
  const { darkMode } = useContext(DarkModeContext);
  const useStyle = makeStyles(theme => ({
    root: {
      width: "300px",
      backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
      marginLeft: theme.spacing(1)
    }
  }));
  const classes = useStyle();
  return (
        <Grid
          item
          xs={12}
          sm={"auto"}
          xl={"auto"}
          
        >
    <Draggable draggableId={list.id} index={index}>
      {provided => (
          <div style={{ padding: 10 }}{...provided.draggableProps}
          ref={provided.innerRef}>
            <Paper className={classes.root}  {...provided.dragHandleProps}>
              <CssBaseline />
              <Title list={list} collection="lists" />
              <Droppable droppableId={list.id}>
                {provided => (
                  <div
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    className={classes.cardContainer}
                  >
                    {provided.placeholder}
                    {list.cards.map((card, i) => (
                      <Card key={card.id} card={card} index={i} list={list} />
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
              <InputContainer
                listId={list.documentId}
                listCards={list.cards}
                boardId={list.boardId}
              />
            </Paper>
          </div>
      )}
    </Draggable>
        </Grid>
  );
};

export default List;
