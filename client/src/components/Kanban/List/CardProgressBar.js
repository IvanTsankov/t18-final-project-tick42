import React, { useContext } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import DarkModeContext from '../../../providers/DarkModeContext';


const CardProgressBar = ({ fullwidth, checkList })  => {
  const { darkMode } = useContext(DarkModeContext);
  const progressValue= (checkList.filter(checkbox => checkbox.isDone).length/ checkList.length) || 0

    const BorderLinearProgress = withStyles((theme) => ({
        root: {
          borderRadius: 4,
          width: fullwidth ? '100%' : 70,
          height: fullwidth ? '10px' : '5px',
          margin: fullwidth ? '5px 0px 5px 0px' : '0px'
        },
        colorPrimary: {
          backgroundColor: theme.palette.grey[darkMode ? 200 : 700],
        },
        bar: {
          borderRadius: 4,
          backgroundColor: (progressValue * 100) <= 34 ? '#DE1A1A' : (progressValue * 100) >= 67 ? '#35FF69' : '#FCE836',
        },
      }))(LinearProgress);
      
      const useStyles = makeStyles({
        root: {
          flexGrow: 1,
        },
      });
      

  const classes = useStyles();

  

  return (
    <div className={classes.root}>
      <BorderLinearProgress variant="determinate" value={progressValue * 100} />
    </div>
  );
}

export default CardProgressBar;