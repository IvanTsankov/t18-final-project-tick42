import React, { useContext } from "react";
import DrawingTools from "./DrawingTools";
import TextFieldTools from "./TextFieldTools";
import { Paper, Button } from "@material-ui/core";
import DarkModeContext from "./../../../providers/DarkModeContext";
import CloseIcon from "@material-ui/icons/Close";

const Toolbar = ({
  tool,
  setTool,
  selected,
  setSelected,
  setTexts,
  texts,
  elements,
  setElements,
  setDrawing,
  drawing,
  setCurrentColor,
  currentColor,
  setCurrentThickness,
  currentThickness,
  updateElement
}) => {
  const { darkMode } = useContext(DarkModeContext);

  return (
      <div style={{ display: "inline-block",
      width: "auto",
      height: "auto",
      marginLeft: 300}}>
      <Button onClick={() => setTool("")}>
        <CloseIcon
          style={{
            color: darkMode ? "#F0F2F5" : "#18191A"
          }}
        />
      </Button>
    <Paper
      elevation={3}
      style={{
        backgroundColor: darkMode ? "#18191A" : "#F0F2F5",
      }}
    >
      {(tool === "drawing" || tool === 'erase') && (
        <DrawingTools
          setDrawing={setDrawing}
          drawing={drawing}
          setCurrentThickness={setCurrentThickness}
          currentThickness={currentThickness}
          setCurrentColor={setCurrentColor}
          currentColor={currentColor}
          tool={tool}
        />
      )}
      {(tool === "text" || selected.id) && (
        <TextFieldTools
          selected={selected}
          setSelected={setSelected}
          setTexts={setTexts}
          texts={texts}
          elements={elements}
          setElements={setElements}
          updateElement={updateElement}
        />
      )}
    </Paper>
    </div>
  );
};

export default Toolbar;
