import React, { useState } from "react";
import { ChromePicker } from "react-color";
import colorWeelIcon from "./../../../images/color-wheel.svg";
import thicknessIcon from "./../../../images/thickness.svg";

const DrawingTools = ({
  setDrawing,
  drawing,
  currentThickness,
  setCurrentThickness,
  setCurrentColor,
  currentColor,
  tool,
}) => {
  const [picker, setPicker] = useState("");

  const handleColorChange = color => {
    setDrawing({ ...drawing, color: color.hex });
    setCurrentColor(color.hex);
  };

  const handleThicknessChange = e => {
    setCurrentThickness(e.target.value);
    setDrawing({ ...drawing, lineWidth: currentThickness });
  };

  return (
    <div style={{ paddingLeft: 2, paddingTop: 2, paddingBottom: 2 }}>
      {tool !== 'erase' && <button
        onClick={() => {
          if (picker === "color") {
            setPicker("");
            return;
          }
          setPicker("color");
        }}
        style={{border: 'none', backgroundColor: 'transparent'}}
      >
          <img src={colorWeelIcon} style={{ height: 30}} alt="color"/>
      </button>}
      <button
        onClick={() => {
          if (picker === "thickness") {
            setPicker("");
            return;
          }
          setPicker("thickness");
        }}
        style={{border: 'none', backgroundColor: 'transparent'}}
      >
        <img src={thicknessIcon} style={{ height: 30}} alt="thickness"/>
      </button>
      <div
        style={{
          position: 'absolute', zIndex: 999
        }}
      >
        {(picker === "color" && tool !== 'erase') && (
          <ChromePicker
            color={currentColor}
            disableAlpha
            onChange={handleColorChange}
          />
        )}
        {picker === "thickness" && (
          <>
            <input
              type="range"
              min="1"
              max="30"
              value={currentThickness}
              onChange={handleThicknessChange}
            />
          </>
        )}
      </div>
    </div>
  );
};

export default DrawingTools;
