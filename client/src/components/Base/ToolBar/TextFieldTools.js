import React, { useState, useContext } from "react";
import { ChromePicker } from "react-color";
import { FormControl, NativeSelect } from "@material-ui/core";
import DarkModeContext from "../../../providers/DarkModeContext";
import colorWeelIcon from "./../../../images/color-wheel.svg";
import fontSizeIcon from "./../../../images/fontSize.svg";
import fontIcon from "./../../../images/fontStyle.svg";


const TextFieldTools = ({
  selected,
  setSelected,
  setTexts,
  texts,
}) => {
  const [picker, setPicker] = useState("");
  const { darkMode } = useContext(DarkModeContext);
  const handleColorChange = color => {
    setSelected({ ...selected, color: color.hex });
    setTexts([
      ...texts.map(el => {
        if (el.id === selected.id) {
          return selected;
        }
        return el;
      })
    ]);
  };

  const handleFontSizeChange = e => {
    setSelected({ ...selected, fontSize: +e.target.value });
    setTexts([
      ...texts.map(el => {
        if (el.id === selected.id) {
          return selected;
        }
        return el;
      })
    ]);
  };

  const handleFontChange = e => {
    setTexts([
      ...texts.map(el => {
        if (el.id === selected.id) {
          return {
            ...el,
            fontFamily: e.target.value
          };
        }
        return el;
      })
    ]);
    setSelected({ ...selected, fontFamily: e.target.value });
  };

  return (
    <div style={{ paddingLeft: 2, paddingTop: 2, paddingBottom: 2 }}>
      <button
        onClick={() => {
          if (picker !== "color") {
            setPicker("color");
          }
        }}
        style={{ border: "none", backgroundColor: "transparent" }}
      >
        <img  alt="color" src={colorWeelIcon} style={{ height: 30}}/>
      </button>
      <button
        onClick={() => {
          if (picker !== "font") {
            setPicker("font");
          }
        }}
        style={{ border: "none", backgroundColor: "transparent" }}
      >
        <img src={fontIcon} alt="font" style={{ height: 30}}/>
      </button>
      <button
        onClick={() => {
          if (picker !== "size") {
            setPicker("size");
          }
        }}
        style={{ border: "none", backgroundColor: "transparent" }}
      >
        <img alt="size" src={fontSizeIcon} style={{ height: 30}}/>
      </button>
      <div style={{position: 'absolute', zIndex: 999}}>
        {picker === "color" && (
          <ChromePicker
            color={selected.color}
            disableAlpha
            display={false}
            onChange={handleColorChange}
          />
        )}
        {picker === "font" && (
            <div style={{position: "realtive",
            borderRadius: "4px", zIndex: 999}}>
              <FormControl value={selected.fontFamily}>
              <NativeSelect
                style={{
                width: 162,
                borderRadius: "4px",
                  backgroundColor: darkMode ? "#F0F2F5" : "#18191A",
                  color: darkMode ? "#18191A" : "#F0F2F5",
                }}
                onChange={handleFontChange}
                value={selected.fontFamily}
              >
          <option value={"Helvetica"}> Helvetica</option>
                <option value={"Calibri"}>Calibri</option>
                <option value={"Futura"}>Futura</option>
                <option value={"Garamond"}>Garamond</option>
                <option value={"Times New Roman"}>Times New Roman</option>
                <option value={"Verdana"}>Verdana</option>
        </NativeSelect>
                
              </FormControl>
            </ div>
        )}
        {picker === "size" && (
          <input
            type="number"
            value={selected.fontSize}
            min="10"
            max="90"
            onChange={handleFontSizeChange}
          />
        )}
        {/* {picker !== "" && <button onClick={handleColorClose}>X</button>} */}
      </div>
    </div>
  );
};

export default TextFieldTools;
