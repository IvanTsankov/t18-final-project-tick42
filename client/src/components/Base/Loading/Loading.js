import React from "react";
import './Loading.css';

const Loading = () => {

  return (
    <h1 className='loading'>
      <span>L</span>
      <span>o</span>
      <span>a</span>
      <span>d</span>
      <span>i</span>
      <span>n</span>
      <span>g</span>
      <span>.</span>
      <span>.</span>
      <span>.</span>
    </h1>
  );
};

export default Loading;
