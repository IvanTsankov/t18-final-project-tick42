import React, { useContext } from 'react';
import application from '../../../base';
import { Grid } from '@material-ui/core';
import ChatContext from '../../../providers/ChatContext';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import * as toastr from 'toastr';

const CustomLogoutButton = ({ history }) => {
    const { chat, setChat } = useContext(ChatContext);
    const { user } = useContext(AuthContext);

    const logout = async() => {
        setChat({ open: false, name: chat.name, id: chat.id });
        await application
        .firestore()
        .collection('mouses')
        .doc(user.uid)
        .delete()
        application.auth().signOut();
        localStorage.removeItem("user");
        toastr.success("You have logged out successfully!");
        history.push('/home');
      };

    return (
        <Grid item xs={12} sm={"auto"} xl={"auto"} className="gridNavigationButton">
            <button
                className="profileButton logout navigationButton"
                onClick={logout}
              >
                Sign out
              </button>
        </Grid>
    )
}

export default withRouter(CustomLogoutButton);