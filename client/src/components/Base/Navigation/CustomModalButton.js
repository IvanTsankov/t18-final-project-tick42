import React, { useContext } from 'react';
import { Grid } from '@material-ui/core';
import ChatContext from '../../../providers/ChatContext';

const CustomModalButton = ({ handleModalOpen, buttonName }) => {
    const { chat, setChat } = useContext(ChatContext);
    const handleClick = (e) => {
        e.preventDefault();
        setChat({open: false, name: chat.name, id: chat.id});
        handleModalOpen();
    }
    return (
        <Grid item xs={12} sm={"auto"} xl={"auto"} className="gridNavigationButton">
            <button className={`navigationButton ${buttonName === 'Login' ? 'loginButton' : ''}`} onClick={handleClick}>{buttonName}</button>
        </Grid>
    )
}

export default CustomModalButton;