import React, { useState, useContext, useRef, useEffect } from "react";
import { withRouter } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import AuthForm from "../../Pages/SignIn/AuthForm";
import "./Navigation.scss";
import DarkModeContext from "../../../providers/DarkModeContext";
import CustomButton from "./CustomButton";
import CustomModalButton from "./CustomModalButton";
import { Grid } from "@material-ui/core";
import CustomToggleButton from "./CustomToggleButton";
import MyProfile from "../../Pages/MyProfile/MyProfile";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import application from "../../../base";
import * as toastr from "toastr";
import ChatContext from "../../../providers/ChatContext";
import NotificationsButton from "./NotificationsButton";
import CustomLogoutButton from "./CustomLogoutButton";
import logoLight from "./../../../images/collaborate-light.svg";
import logoDark from "./../../../images/collaborate-dark.svg";

const useStylesNav = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}));

const Navigation = props => {
  const { setChat } = useContext(ChatContext);
  const navClasses = useStylesNav();
  const { darkMode } = useContext(DarkModeContext);
  const user = JSON.parse(localStorage.getItem("user"));
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [menuOpen, setMenuOpen] = useState(false);
  const [profileModalOpen, setProfileModalOpen] = useState(false);
  const anchorRef = useRef(null);
  const prevOpen = useRef(menuOpen);
  const [notifications, setNotifications] = useState([]);
  const [notificationsCount, setNotificationsCount] = useState(0);

  const modalClasses = useStylesModal();

  const handleLoginModalOpen = () => {
    setLoginModalOpen(true);
  };
  const handleLoginModalClose = () => {
    setLoginModalOpen(false);
  };
  const handleProfileModalOpen = () => {
    setProfileModalOpen(true);
  };
  const handleProfileModalClose = () => {
    setProfileModalOpen(false);
  };
  const handleMenuToggle = async () => {
    setMenuOpen(prevOpen => !prevOpen);
    if (0 < notifications.length) {
      const updatedNotifications = notifications.map(el => ({
        ...el,
        isRead: true
      }));
      await application
        .firestore()
        .collection("notifications")
        .doc(user.user.uid)
        .set({ notifications: updatedNotifications });
    }
  };
  const handleMenuClose = (event, el) => {
    setNotificationsCount(0);
    if (el?.chatId) {
      setChat({
        open: true,
        name: el.chatName,
        id: el.chatId
      });
      if (el.chatName === 'General') {
        props.history.push('/services');
      } else if (el?.boardId) {
        props.history.push(`/canvases/${el.boardId}`);
      } else {
        props.history.push(`/kanbans/${el.kanbanId}`);
      }
    }
    else if (el?.boardId) {
      props.history.push(`/canvases/${el.boardId}`);
    } else if(el?.kanbanId) {
      props.history.push(`/kanbans/${el.kanbanId}`);
    }
    setMenuOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setMenuOpen(false);
    }
  }

  useEffect(() => {
    if (prevOpen.current === true && menuOpen === false) {
      anchorRef.current.focus();
    }
    prevOpen.current = menuOpen;

    const fetchNotifications = async () => {
      await application
        .firestore()
        .collection("notifications")
        .where("__name__", "==", user.user.uid)
        .onSnapshot(
          querySnapshot => {
            if (0 < querySnapshot.docs.length) {
              const result = querySnapshot.docs.map(el =>
                el.data().notifications.sort((a,b) => b.time-a.time)
              );
              setNotifications(...result);
              const readNotifications = result[0].filter(el => !el.isRead)
                .length;
              setNotificationsCount(readNotifications);
              const newTitle = readNotifications
                ? `(${readNotifications}) Collaborate`
                : "Collaborate";
              document.title = `${newTitle}`;
            }
          },
          err => toastr.error(err)
        );
    };

    user && fetchNotifications();

    return async () => {
      await application
        .firestore()
        .collection("notifications")
        .where("__name__", "==", user.user.uid)
        .onSnapshot(() => {});
    };
  }, []);

  return (
    <div className={navClasses.root}>
      <AppBar
        position="relative"
        style={{
          background: "transparent",
          boxShadow: "none"
        }}
        className="navigationBar"
      >
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            spacing={1}
          >
            <Typography variant="h6" className={navClasses.title + " logo"}>
              <img
                src={darkMode ? logoDark : logoLight}
                style={{ height: "auto", width: "200px" }}
                alt="logo"
              />
            </Typography>
            <CustomToggleButton />
            <CustomButton
              route={"/home"}
              buttonName={"Home"}
              path={props.location.pathname}
            />
            {user ? (
              <>
                <CustomButton
                  color="inherit"
                  route={"/services"}
                  buttonName={"Services"}
                  path={props.location.pathname}
                />
                <CustomModalButton
                  handleModalOpen={handleProfileModalOpen}
                  buttonName={"Profile"}
                />
                <CustomLogoutButton />
                <NotificationsButton
                  handleMenuToggle={handleMenuToggle}
                  anchorRef={anchorRef}
                  menuOpen={menuOpen}
                  notificationsCount={notificationsCount}
                />
                <Popper
                  open={menuOpen}
                  anchorEl={anchorRef.current}
                  role={undefined}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={handleMenuClose}>
                          <MenuList
                            autoFocusItem={menuOpen}
                            id="menu-list-grow"
                            onKeyDown={handleListKeyDown}
                          >
                            {notifications.map((el, i) => (
                              <MenuItem
                                key={i}
                                onClick={e => handleMenuClose(e, el)}
                              >
                                {el.content}{" "}
                              </MenuItem>
                            ))}
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </>
            ) : (
              <CustomModalButton
                handleModalOpen={handleLoginModalOpen}
                buttonName={"Login"}
              />
            )}
          </Grid>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={
              modalClasses.modal + (darkMode ? " dark-mode" : " light-mode")
            }
            open={loginModalOpen}
            onClose={handleLoginModalClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <>
              <AuthForm
                in={loginModalOpen}
                handleModalClose={handleLoginModalClose}
              />
            </>
          </Modal>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={
              modalClasses.modal + (darkMode ? " dark-mode" : " light-mode")
            }
            open={profileModalOpen}
            onClose={handleProfileModalClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <>
              <MyProfile
                in={profileModalOpen}
                handleModalClose={handleProfileModalClose}
              />
            </>
          </Modal>
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default withRouter(Navigation);
