import React, { useContext } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import ChatContext from '../../../providers/ChatContext';

const CustomButton = ({buttonName, route, history}) => {
    const { chat, setChat } = useContext(ChatContext);
    return (
        <Grid item xs={12} sm={"auto"} xl={"auto"} className="gridNavigationButton">
            <button onClick={() => { setChat({open:false, name: chat.name, id: chat.id}); history.push(route)}} className="navigationButton">{buttonName}</button>
        </Grid>
    )
}

export default withRouter(CustomButton);