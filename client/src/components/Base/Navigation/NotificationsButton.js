import React from 'react';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Grid } from "@material-ui/core";

const NotificationsButton = ({handleMenuToggle, anchorRef, menuOpen, notificationsCount }) => {

    return (
        <Grid item xs={12} sm={"auto"} xl={"auto"} className="gridNavigationButton">
            <button className="navigationButton" style={{height: '64px', width: '80px', marginRight: '0px'}} ref={anchorRef} aria-controls={menuOpen ? 'menu-list-grow' : undefined} aria-haspopup="true" onClick={handleMenuToggle}>
              <Badge badgeContent={notificationsCount} color="secondary">
                <NotificationsIcon style={{height: '30px', width: '30px'}} />
              </Badge>
            </button>
        </Grid>
    )
}

export default NotificationsButton;