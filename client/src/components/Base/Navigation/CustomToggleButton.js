import React, { useContext } from "react";
import DarkModeContext from "../../../providers/DarkModeContext";
import { Grid } from "@material-ui/core";

const CustomToggleButton = () => {
  const { darkMode, setDarkMode } = useContext(DarkModeContext);
  const handleChange = () => {
    localStorage.setItem("dark", !darkMode);
    setDarkMode(!darkMode);
  };

  return (
    <Grid item xs={12} sm={"auto"} xl={"auto"}>
      <label className="switchB">
        <input type="checkbox" checked={darkMode} onChange={handleChange}/>
        <div>
            <span></span>
        </div>
      </label>
    </Grid>
  )
};

export default CustomToggleButton;
