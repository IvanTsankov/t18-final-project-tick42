import React, { useState, useEffect, useContext } from "react";
import Editor from "draft-js-plugins-editor";
import createMentionPlugin, {
  defaultSuggestionsFilter
} from "draft-js-mention-plugin";
import "draft-js/dist/Draft.css";
import "draft-js-mention-plugin/lib/plugin.css";
import application from "../../../base";
import * as toastr from "toastr";
import createEmojiPlugin from "draft-js-emoji-plugin";
import ChatContext from "../../../providers/ChatContext";
import { withRouter } from 'react-router-dom';

const emojiPlugin = createEmojiPlugin({
  useNativeArt: true,
  positionSuggestions: settings => {
    return {
      display: "block",
      transform: "scale(1) translateY(-100%) translateX(-100%)", 
      background: '#347cdb',
      height: '150px',
      width: '200px',
      position: 'relative',
      zIndex: '100'
    };
  }
});
const { EmojiSuggestions } = emojiPlugin;

const mentionPlugin = createMentionPlugin({
  mentionPrefix: "@",
  positionSuggestions: settings => {
    return {
      transform: "scale(1) translateY(-72%) translateX(-100%)",
      border: 0,
      background: '#347cdb',
    };
  }
});
const { MentionSuggestions } = mentionPlugin;
const plugins = [mentionPlugin, emojiPlugin];

const ChatInputField = ({ handleSubmit, editorState, setEditorState, location }) => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [suggestions, setSuggestions] = useState([]);

  const onSearchChange = ({ value }) => {
    setSuggestions(defaultSuggestionsFilter(value, suggestions));
  };

  const { chat } = useContext(ChatContext);
  let currentLocation = location.pathname.includes('canvases') ? 'canvases' : location.pathname.includes('kanbans') ? 'kanbans' : 'General';
  useEffect(() => {
    const fetchUsers = async () => {
      let arr = [];
      await application
        .firestore()
        .collection("users")
        .where("__name__", "!=", user.user.uid)
        .get()
        .then(
          data => {
            arr = data.docs.map(data => ({ ...data.data(), id: data.id }));
          },
          err => toastr.error(err)
        );
      setSuggestions(arr);
    };

    const fetchUsersByBoard = async () => {
      await application
        .firestore()
        .collection(currentLocation)
        .doc(chat.id)
        .get()
        .then(
          async data => {
            if(data.data()?.isPublic) {
              fetchUsers();
              return;
            }
            const userIds = data.data()?.invitedUsers;
            let usersSuggestions = [];
            if(userIds?.length === 0) return;
            await application
              .firestore()
              .collection("users")
              .where("__name__", "in", userIds)
              .get()
              .then(
                data => {
                  usersSuggestions = data.docs.map(data => ({
                    ...data.data(),
                    id: data.id
                  }));
                },
                err => toastr.error(err)
              );
            setSuggestions(usersSuggestions);
          },
          err => toastr.error(err)
        );
    };

    if (chat.name !== 'General') {
      fetchUsersByBoard();
    } else {
      fetchUsers();
    }
  }, []);

  const onAddMention = e => {};

  return (
    <>
      <div style={{width: '360px', display: 'inline-block', float: 'left'}}>
        <Editor
          editorState={editorState}
          plugins={plugins}
          onChange={editorState => {
            setEditorState(editorState);
          }}
          placeholder={"Type here..."}
          handleReturn={handleSubmit}
        />
        <EmojiSuggestions />
      </div>

      <MentionSuggestions
        onSearchChange={onSearchChange}
        suggestions={suggestions}
        onAddMention={onAddMention}
      />
    </>
  );
};

export default withRouter(ChatInputField);
