import React, { useContext } from 'react';
import AuthContext from './../../../providers/AuthContext';
import { Grid } from "@material-ui/core";
import { EditorState, convertFromRaw } from "draft-js";
import Editor from "draft-js-plugins-editor";
import createEmojiPlugin from "draft-js-emoji-plugin";
import createMentionPlugin from "draft-js-mention-plugin";


const emojiPlugin = createEmojiPlugin({
    useNativeArt: true,
    positionSuggestions: settings => {
      return {
        display: "block",
        transform: "scale(1) translateY(-100%)",
        transformOrigin: "1em 0% 0px",
        transition: "all 0.25s cubic-bezier(0.3, 1.2, 0.2, 1)",
      };
    }
  });
  
  const mentionPlugin = createMentionPlugin({
    mentionPrefix: "@"
  });
  const plugins = [mentionPlugin, emojiPlugin];

const ChatMessage = ({ message, prevAuthor }) => {
    const { user } = useContext(AuthContext);
    if(user.displayName === message.author) {

        if(prevAuthor) {
            return (
                <Grid item xs={12} sm={12} xl={12} style={{padding: '0px 8px'}}>
                <div className="chatMessageOuter outgoingMessage">
                    <div className="chatMessageInner chatMessageOutgoing">
                        <span className="chatMessageMessage">
                            <Editor
                            editorState={EditorState.createWithContent(convertFromRaw(message.content))}
                            readOnly={true}
                            plugins={plugins}
                            onChange={() => {}}
                            />
                        </span>
                    </div>
                </div>
            </Grid>
            )
        }
        
        return (
            <Grid item xs={12} sm={12} xl={12} style={{padding: '0px 8px'}}>
                <div className="chatMessageOuter outgoingMessage">
                    <div className="chatMessageInner chatMessageOutgoing">
                        <span className="chatMessageSender">
                            {message.author}
                        </span>
                        <span className="chatMessageMessage">
                            <br/>
                            <Editor
                            editorState={EditorState.createWithContent(convertFromRaw(message.content))}
                            readOnly={true}
                            plugins={plugins}
                            onChange={() => {}}
                            />
                        </span>
                    </div>
                </div>
            </Grid>
        )
    }
    if(prevAuthor) {
        return (
            <Grid item xs={12} sm={12} xl={12} style={{padding: '0px 8px'}}>
                <div className="chatMessageOuter incomingMessage">
                    <div className="chatMessageInner chatMessageIncoming">
                        <span className="chatMessageMessage">
                            <Editor
                            editorState={EditorState.createWithContent(convertFromRaw(message.content))}
                            readOnly={true}
                            plugins={plugins}
                            onChange={() => {}}
                            />
                        </span>
                    </div>
                </div>
            </Grid>
        )
    }
    return (
        <Grid item xs={12} sm={12} xl={12} style={{padding: '0px 8px'}}>
            <div className="chatMessageOuter incomingMessage">
                <div className="chatMessageInner chatMessageIncoming">
                    <span className="chatMessageSender">
                        {message.author}
                    </span>
                    <span className="chatMessageMessage">
                        <br/>
                        <Editor
                        editorState={EditorState.createWithContent(convertFromRaw(message.content))}
                        readOnly={true}
                        plugins={plugins}
                        onChange={() => {}}
                        />
                    </span>
                    
                </div>
            </div>
        </Grid>
    )
}

export default ChatMessage;