import React, { useState, useEffect, useContext } from "react";
import "./Chat.scss";
import { Grid } from "@material-ui/core";
import ChatMessage from "./ChatMessage";
import Button from "@material-ui/core/Button";
import DarkModeContext from "./../../../providers/DarkModeContext";
import ChatContext from "./../../../providers/ChatContext";
import application from "../../../base";
import * as toastr from "toastr";
import ChatInputField from "./ChatInputField";
import { convertToRaw, EditorState } from "draft-js";
import * as firebase from "firebase";
import chatSend from './../../../images/chatSend.svg';
import { withRouter } from 'react-router-dom';

const Chat = ({ location }) => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [messages, setMessages] = useState([]);
  const { darkMode } = useContext(DarkModeContext);
  const { chat, setChat } = useContext(ChatContext);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );

  useEffect(() => {
    const fetchMessages = async () => {
      await application
        .firestore()
        .collection("chats")
        .doc(chat.id)
        .onSnapshot(
          docSnapshot => {
            if (docSnapshot.data()) {
              setMessages(docSnapshot.data().messages);
              document.querySelector(".gridWrapper") &&
                (document.querySelector(
                  ".gridWrapper"
                ).scrollTop = document.querySelector(
                  ".gridWrapper"
                )?.scrollHeight);
            }
          },
          err => toastr.error(err)
        );
    };

    fetchMessages();
    return async () => {
      await application
        .firestore()
        .collection("chats")
        .doc(chat.id)
        .onSnapshot(() => {});
    };
  }, []);
  const handleSubmit = async (e, buttonClicked = false) => {
    const mentions = Object.values(
      convertToRaw(editorState.getCurrentContent()).entityMap
    );
    const message = {
      content: convertToRaw(editorState.getCurrentContent()),
      author: user.user.displayName,
      time: new Date()
    };
    if (
      (e.key === "Enter" || buttonClicked) &&
      !!message.content.blocks[0].text
    ) {
      try {
        await application
          .firestore()
          .collection("chats")
          .doc(chat.id)
          .set({
            messages: [...messages, message]
          });
        setEditorState(() => EditorState.createEmpty());
      } catch (err) {
        toastr.error(err);
      }
    }
    if (0 < mentions.length) {
      const mentionIds = [];
      console.log(location.pathname);
      mentions.forEach(async element => {
        if (
          element.type !== "emoji" &&
          mentionIds.indexOf(element.data?.mention?.id) === -1
        ) {
          await application
            .firestore()
            .collection("notifications")
            .doc(element.data.mention.id)
            .set(
              {
                notifications: firebase.firestore.FieldValue.arrayUnion({
                  chatId: chat.id,
                  chatName: chat.name,
                  content: `${user.user.displayName ||
                    user.user.email.split("@")[0]} mentioned you in ${
                    chat.name
                  } chat!`,
                  time: new Date(),
                  boardId: location.pathname.includes('canvases') ? location.pathname.split('/')[2] : '',
                  kanbanId: location.pathname.includes('kanbans') ? location.pathname.split('/')[2] : '',
                  isRead: false
                })
              },
              { merge: true }
            );
        }
      });
    }
  };

  return (
    <div className="chatWrapper">
      <div className="chatHeader">
        <span className="chatHeaderName">{chat.name}</span>
        <span className="chatHeaderButton">
          <Button
            onClick={() =>
              setChat({ open: false, name: chat.name, id: chat.id })
            }
            style={{ padding: "0px" }}
          >
            X
          </Button>
        </span>
      </div>
      <div className="gridWrapper">
        <Grid container direction="column" spacing={2}>
          {messages.length !== 0 ? (
            messages.map((message, i) => (
              <ChatMessage key={i} message={message} prevAuthor={messages[i-1]?.author === messages[i].author} />
            ))
          ) : (
            <Grid item xs={12} sm={"auto"} xl={"auto"}>
              <span
                style={{ color: darkMode ? "rgb(218,218,218)" : "rgb(0,0,0)" }}
              >
                No messages to display
              </span>
            </Grid>
          )}
        </Grid>
      </div>
      <div className="inputMessageOuter">
        <div className="inputMessageInner">
          <ChatInputField messages={messages} handleSubmit={handleSubmit}  editorState={editorState} setEditorState={setEditorState}/>
          <button onClick={(e) => handleSubmit(e, true)}><img src={chatSend} alt='chat send' className="chatSend"/></button>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Chat);
