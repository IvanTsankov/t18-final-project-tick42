import React, { useState, useEffect } from "react";
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import "./common/toastr-service";
import AuthContext from "./providers/AuthContext";
import GuardedRoute from "./providers/GuardedRoute";
import Canvas from "./components/Drawing/Canvas";
import Home from "./components/Pages/Home/Home";
import Layout from "./components/Layout/Layout";
import DarkModeContext, { getInitialMode } from "./providers/DarkModeContext";
import { Helmet } from "react-helmet";
import application from "./base";
import Services from "./components/Pages/Services/Services";
import NotFound from './components/Pages/NotFound/NotFound';
import { Container } from "@material-ui/core";
import ChatContext from "./providers/ChatContext";
import './App.scss';
import './variables.scss';
import KanbanBoard from "./components/Kanban/KanbanBoard";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

const App = () => {
  const [darkMode, setDarkMode] = useState(getInitialMode());
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));
  const [chat, setChat] = useState({ open: false, name: 'General', id: 'generalkj12bhk3b12k3h2J121'});
  useEffect(() => {
    application.auth().onAuthStateChanged(setUser);

  }, []);

  return (
      <div className={darkMode ? "dark-mode" : "light-mode"}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <Container maxWidth="xl">
              <Helmet><style>{`body { background: ${darkMode ? `linear-gradient(90deg, rgba(58,59,60,1) 0%, rgba(24,25,26,1) 45%, rgba(24,25,26,1) 55%, rgba(58,59,60,1) 100%)` : `linear-gradient(90deg, rgba(250,250,250,1) 0%, rgba(229,229,229,1) 40%, rgba(229,229,229,1) 60%, rgba(250,250,250,1) 100%)`}; }`}</style></Helmet>
              <BrowserRouter>
                <DarkModeContext.Provider value={{ darkMode, setDarkMode }}>
                    <AuthContext.Provider value={{ user, setUser }}>
                      <ChatContext.Provider value={{ chat, setChat }}>
                        <Layout>
                          <Switch>
                            <Redirect from="/" exact to="/home" />
                            <Route path="/home" component={Home} />
                            <GuardedRoute path="/canvases/:id" auth={!!user} component={Canvas} />
                            <GuardedRoute path="/services/" auth={!!user} component={Services} />
                            <GuardedRoute path="/kanbans/:id" auth={!!user} component={KanbanBoard} />
                            <Route path="*" component={NotFound} />
                          </Switch>
                        </Layout>
                      </ChatContext.Provider>
                    </AuthContext.Provider>
                </DarkModeContext.Provider>
              </BrowserRouter>
          </Container>
        </MuiPickersUtilsProvider>
      </div>
  );
};

export default App;
